import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { OurTeamsComponent } from './our-teams/our-teams.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [			
      AppComponent,
      HomeComponent,
      AboutUsComponent,
      OurTeamsComponent
   ],
  imports: [BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
