import { Injectable, isDevMode, Inject, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngxs/store';
import { take } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';
import { LoadAppSpecificConfiguration } from '../../state/app.actions';
import { IAppSpecificConfiguration } from '../../models/configuration.model';
import { LoadConfiguration } from '@fluctware/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  constructor(private http: HttpClient, private store: Store, @Inject(PLATFORM_ID) private platformId: Object) { }
  getProcessEnvConfiguration() {
    if (!isDevMode() && isPlatformBrowser(this.platformId)) {
      const baseURI = document.baseURI.endsWith('/')
        ? document.baseURI
        : `${document.baseURI}/`;
      const url = `${baseURI}api/configuration`;

      this.http
        .get(url)
        .pipe(take(1))
        .subscribe((x: IAppSpecificConfiguration) => {
          this.store.dispatch(new LoadConfiguration(x));
          this.store.dispatch(new LoadAppSpecificConfiguration(x));
        });
    }
  }
}
