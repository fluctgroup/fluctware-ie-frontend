import { TestBed, inject } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';

import { NgxsModule } from '@ngxs/store';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthModule } from 'angular-auth-oidc-client';
import { CoreState } from '@fluctware/core';
import { ConfigurationService } from './configuration.service';

describe('ConfigurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ConfigurationService
      ],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        AuthModule.forRoot(),
        NgxsModule.forRoot([CoreState])
      ]
    });
  });

  it('should be created',
    inject([ConfigurationService], (service: ConfigurationService) => {
      expect(service).toBeTruthy();
    })
  );
});
