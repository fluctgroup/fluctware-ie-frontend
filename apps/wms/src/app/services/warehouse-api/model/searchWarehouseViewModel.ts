/**
 * Fluctware - Warehouse HTTP API
 * Warehouse Microservice HTTP API .
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

export interface SearchWarehouseViewModel { 
    areaLevel1?: string;
    areaLevel2?: string;
    areaLevel3?: string;
    startDate?: Date;
    endDate?: Date;
    spaceRequired?: number;
}