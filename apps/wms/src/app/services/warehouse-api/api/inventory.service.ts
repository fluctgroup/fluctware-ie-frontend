/**
 * Fluctware - Warehouse HTTP API
 * Warehouse Microservice HTTP API .
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *//* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { CheckStockByExpireViewModel } from '../model/checkStockByExpireViewModel';
import { CheckWarehouseStockViewModel } from '../model/checkWarehouseStockViewModel';
import { EditStockCommand } from '../model/editStockCommand';
import { InventoryHistoryViewModel } from '../model/inventoryHistoryViewModel';
import { InventoryViewModel } from '../model/inventoryViewModel';
import { StockExpiryPairViewModel } from '../model/stockExpiryPairViewModel';
import { ValidationViewModel } from '../model/validationViewModel';
import { WarehouseStockPairViewModel } from '../model/warehouseStockPairViewModel';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class InventoryService {

    protected basePath = '/';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * 
     * 
     * @param body 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiInventoryCheckStockByExpirePost(body?: CheckStockByExpireViewModel, observe?: 'body', reportProgress?: boolean): Observable<Array<WarehouseStockPairViewModel>>;
    public apiInventoryCheckStockByExpirePost(body?: CheckStockByExpireViewModel, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<WarehouseStockPairViewModel>>>;
    public apiInventoryCheckStockByExpirePost(body?: CheckStockByExpireViewModel, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<WarehouseStockPairViewModel>>>;
    public apiInventoryCheckStockByExpirePost(body?: CheckStockByExpireViewModel, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<Array<WarehouseStockPairViewModel>>('post',`${this.basePath}/api/Inventory/check-stock/by-expire`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiInventoryCheckStockWarehousePost(body?: CheckWarehouseStockViewModel, observe?: 'body', reportProgress?: boolean): Observable<Array<StockExpiryPairViewModel>>;
    public apiInventoryCheckStockWarehousePost(body?: CheckWarehouseStockViewModel, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<StockExpiryPairViewModel>>>;
    public apiInventoryCheckStockWarehousePost(body?: CheckWarehouseStockViewModel, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<StockExpiryPairViewModel>>>;
    public apiInventoryCheckStockWarehousePost(body?: CheckWarehouseStockViewModel, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<Array<StockExpiryPairViewModel>>('post',`${this.basePath}/api/Inventory/check-stock/warehouse`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiInventoryEditPut(body?: EditStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiInventoryEditPut(body?: EditStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiInventoryEditPut(body?: EditStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiInventoryEditPut(body?: EditStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('put',`${this.basePath}/api/Inventory/edit`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiInventoryStockHistoryIdGet(id: number, observe?: 'body', reportProgress?: boolean): Observable<Array<InventoryHistoryViewModel>>;
    public apiInventoryStockHistoryIdGet(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<InventoryHistoryViewModel>>>;
    public apiInventoryStockHistoryIdGet(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<InventoryHistoryViewModel>>>;
    public apiInventoryStockHistoryIdGet(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling apiInventoryStockHistoryIdGet.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<Array<InventoryHistoryViewModel>>('get',`${this.basePath}/api/Inventory/stock/history/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiInventoryStockIdGet(id: number, observe?: 'body', reportProgress?: boolean): Observable<InventoryViewModel>;
    public apiInventoryStockIdGet(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<InventoryViewModel>>;
    public apiInventoryStockIdGet(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<InventoryViewModel>>;
    public apiInventoryStockIdGet(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling apiInventoryStockIdGet.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<InventoryViewModel>('get',`${this.basePath}/api/Inventory/stock/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiInventoryWarehouseIdGet(id: number, observe?: 'body', reportProgress?: boolean): Observable<Array<InventoryViewModel>>;
    public apiInventoryWarehouseIdGet(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<InventoryViewModel>>>;
    public apiInventoryWarehouseIdGet(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<InventoryViewModel>>>;
    public apiInventoryWarehouseIdGet(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling apiInventoryWarehouseIdGet.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<Array<InventoryViewModel>>('get',`${this.basePath}/api/Inventory/warehouse/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
