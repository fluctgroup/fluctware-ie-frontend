/**
 * Fluctware - Warehouse HTTP API
 * Warehouse Microservice HTTP API .
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *//* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { ApproveRequestInboundStockCommand } from '../model/approveRequestInboundStockCommand';
import { ApproveRequestOutboundStockCommand } from '../model/approveRequestOutboundStockCommand';
import { CancelRequestTransportCommand } from '../model/cancelRequestTransportCommand';
import { InboundRequestedStockCommand } from '../model/inboundRequestedStockCommand';
import { InboundStockCommand } from '../model/inboundStockCommand';
import { OutboundRequestedStockCommand } from '../model/outboundRequestedStockCommand';
import { OutboundStockCommand } from '../model/outboundStockCommand';
import { RejectRequestInboundStockCommand } from '../model/rejectRequestInboundStockCommand';
import { RejectRequestOutboundStockCommand } from '../model/rejectRequestOutboundStockCommand';
import { RequestInboundStockCommand } from '../model/requestInboundStockCommand';
import { RequestOutboundStockCommand } from '../model/requestOutboundStockCommand';
import { ReturnRequestCommand } from '../model/returnRequestCommand';
import { TransportViewModel } from '../model/transportViewModel';
import { ValidationViewModel } from '../model/validationViewModel';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class TransportService {

    protected basePath = '/';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * 
     * 
     * @param id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportDetailIdGet(id: number, observe?: 'body', reportProgress?: boolean): Observable<TransportViewModel>;
    public apiTransportDetailIdGet(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<TransportViewModel>>;
    public apiTransportDetailIdGet(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<TransportViewModel>>;
    public apiTransportDetailIdGet(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling apiTransportDetailIdGet.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<TransportViewModel>('get',`${this.basePath}/api/Transport/detail/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportHistoryIdGet(id: number, observe?: 'body', reportProgress?: boolean): Observable<Array<TransportViewModel>>;
    public apiTransportHistoryIdGet(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<TransportViewModel>>>;
    public apiTransportHistoryIdGet(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<TransportViewModel>>>;
    public apiTransportHistoryIdGet(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling apiTransportHistoryIdGet.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<Array<TransportViewModel>>('get',`${this.basePath}/api/Transport/history/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportInboundPost(body?: InboundStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportInboundPost(body?: InboundStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportInboundPost(body?: InboundStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportInboundPost(body?: InboundStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/inbound`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportOngoingIdGet(id: number, observe?: 'body', reportProgress?: boolean): Observable<Array<TransportViewModel>>;
    public apiTransportOngoingIdGet(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<TransportViewModel>>>;
    public apiTransportOngoingIdGet(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<TransportViewModel>>>;
    public apiTransportOngoingIdGet(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling apiTransportOngoingIdGet.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<Array<TransportViewModel>>('get',`${this.basePath}/api/Transport/ongoing/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportOutboundPost(body?: OutboundStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportOutboundPost(body?: OutboundStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportOutboundPost(body?: OutboundStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportOutboundPost(body?: OutboundStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/outbound`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqCancelPost(body?: CancelRequestTransportCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqCancelPost(body?: CancelRequestTransportCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqCancelPost(body?: CancelRequestTransportCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqCancelPost(body?: CancelRequestTransportCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/cancel`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqInboundApprovePost(body?: ApproveRequestInboundStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqInboundApprovePost(body?: ApproveRequestInboundStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqInboundApprovePost(body?: ApproveRequestInboundStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqInboundApprovePost(body?: ApproveRequestInboundStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/inbound/approve`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqInboundOutDeliveryPost(body?: InboundRequestedStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqInboundOutDeliveryPost(body?: InboundRequestedStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqInboundOutDeliveryPost(body?: InboundRequestedStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqInboundOutDeliveryPost(body?: InboundRequestedStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/inbound/out-delivery`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqInboundPost(body?: RequestInboundStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqInboundPost(body?: RequestInboundStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqInboundPost(body?: RequestInboundStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqInboundPost(body?: RequestInboundStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/inbound`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqInboundRejectPost(body?: RejectRequestInboundStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqInboundRejectPost(body?: RejectRequestInboundStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqInboundRejectPost(body?: RejectRequestInboundStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqInboundRejectPost(body?: RejectRequestInboundStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/inbound/reject`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqOutboundApprovePost(body?: ApproveRequestOutboundStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqOutboundApprovePost(body?: ApproveRequestOutboundStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqOutboundApprovePost(body?: ApproveRequestOutboundStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqOutboundApprovePost(body?: ApproveRequestOutboundStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/outbound/approve`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqOutboundOutDeliveryPost(body?: OutboundRequestedStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqOutboundOutDeliveryPost(body?: OutboundRequestedStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqOutboundOutDeliveryPost(body?: OutboundRequestedStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqOutboundOutDeliveryPost(body?: OutboundRequestedStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/outbound/out-delivery`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqOutboundPost(body?: RequestOutboundStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqOutboundPost(body?: RequestOutboundStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqOutboundPost(body?: RequestOutboundStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqOutboundPost(body?: RequestOutboundStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/outbound`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqOutboundRejectPost(body?: RejectRequestOutboundStockCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqOutboundRejectPost(body?: RejectRequestOutboundStockCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqOutboundRejectPost(body?: RejectRequestOutboundStockCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqOutboundRejectPost(body?: RejectRequestOutboundStockCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/outbound/reject`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * 
     * @param body 
     * @param xRequestid 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiTransportReqReturnPost(body?: ReturnRequestCommand, xRequestid?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiTransportReqReturnPost(body?: ReturnRequestCommand, xRequestid?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiTransportReqReturnPost(body?: ReturnRequestCommand, xRequestid?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiTransportReqReturnPost(body?: ReturnRequestCommand, xRequestid?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let headers = this.defaultHeaders;
        if (xRequestid !== undefined && xRequestid !== null) {
            headers = headers.set('x-requestid', String(xRequestid));
        }

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'text/plain',
            'application/json',
            'text/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json',
            'text/json',
            'application/_*+json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.request<any>('post',`${this.basePath}/api/Transport/req/return`,
            {
                body: body,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
