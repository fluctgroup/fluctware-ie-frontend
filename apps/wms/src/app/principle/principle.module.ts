import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrincipleFormComponent } from './principle-form/principle-form.component';
import { SharedModule } from '@fluctware/shared';
import { NgxsModule } from '@ngxs/store';
import { PrincipleState } from './state/principle.state';
import { PrincipleRoutingModule } from './principle-routing.module';
import { PrincipleAddComponent } from './principle-add/principle-add.component';
import { PrincipleEditComponent } from './principle-edit/principle-edit.component';
import { PrincipleListComponent } from './principle-list/principle-list.component';
import { PrincipleDashboardComponent } from './principle-dashboard/principle-dashboard.component';
import { ApiModule } from '../services/warehouse-api';
import { PrincipleDashboardTableComponent } from './principle-dashboard-table/principle-dashboard-table.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ApiModule,
    NgxsModule.forFeature([PrincipleState]),
    PrincipleRoutingModule
  ],
  declarations: [PrincipleFormComponent,
    PrincipleAddComponent,
    PrincipleEditComponent,
    PrincipleDashboardComponent,
    PrincipleDashboardTableComponent,
  PrincipleListComponent]
})
export class PrincipleModule { }
