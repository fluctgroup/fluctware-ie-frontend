import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EditPrinciple } from '../../principle/state/principle.actions';
import { PrincipleViewModel } from '../../services/warehouse-api';
import { FetchPrincipleById } from '../state/principle.actions';
import { PrincipleState } from '../state/principle.state';

@Component({
  selector: 'fluctware-principle-edit',
  templateUrl: './principle-edit.component.html',
  styleUrls: ['./principle-edit.component.scss']
})
export class PrincipleEditComponent implements OnInit, OnDestroy {

  @Select(PrincipleState.selectedPrinciple)
  principle$: Observable<PrincipleViewModel>;
  
  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  itemId: number;
  
  unsubscribe$: ReplaySubject<boolean>;

  constructor(private route: ActivatedRoute, private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  ngOnInit() {
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
      if (y) {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.itemId = Number(params.get('id'));
          this.store.dispatch(new FetchPrincipleById(this.itemId));
        });
      }
    })
  }
  submit(principle:PrincipleViewModel){
    this.store.dispatch(new EditPrinciple(principle));
  }
}