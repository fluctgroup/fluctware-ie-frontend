import { PrincipleViewModel } from "../../services/warehouse-api";

export class AddPrinciple {
  static readonly type = '[WMS] ADD PRINCIPLE';
  constructor(public payload: PrincipleViewModel) { }
}
export class EditPrinciple {
  static readonly type = '[WMS] EDIT PRINCIPLE';
  constructor(public payload: PrincipleViewModel) { }
}

export class DeletePrinciple {
  static readonly type = '[WMS] DELETE PRINCIPLE';
  constructor(public payload: PrincipleViewModel) { }
}

export class FetchPrincipleById {
  static readonly type = '[WMS] FETCH PRINCIPLE BY ID';
  constructor(public payload: number) { }
}
export class FetchPrincipleInventoryById {
  static readonly type = '[WMS] FETCH PRINCIPLE INVENTORY BY ID';
  constructor(public payload: number) { }
}
export class FetchPrinciple {
  static readonly type = '[WMS] FETCH PRINCIPLE';
  constructor() { }
}