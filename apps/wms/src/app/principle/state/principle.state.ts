import { State, Selector, StateContext, Action, Select } from '@ngxs/store';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CoreState, IConfiguration, RegisterLoadingOverlay, Alert, ResolveLoadingOverlay, Navigate, SessionStorageService, Guid } from '@fluctware/core';
import { Observable, ReplaySubject } from 'rxjs';
import { AddPrinciple, DeletePrinciple, EditPrinciple, FetchPrinciple, FetchPrincipleById, FetchPrincipleInventoryById } from './principle.actions';
import { Injectable } from '@angular/core';
import { take, tap, mergeMap, takeUntil } from 'rxjs/operators';
import { Configuration, PrincipleInventoryViewModel, PrincipleService, PrincipleViewModel } from '../../services/warehouse-api';

export interface PrincipleStateModel {
  principles: PrincipleViewModel[];
  principleItems: PrincipleInventoryViewModel[];
  selectedPrinciple:PrincipleViewModel;
}
const defaults: PrincipleStateModel = {
  principles:[],
  principleItems:[],
  selectedPrinciple:null
};
@State<PrincipleStateModel>({
  name: 'Principle',
  defaults: defaults
})
@Injectable()
export class PrincipleState {

  @Select(CoreState.configuration) 
  configurations$: Observable<IConfiguration>;

  configuration: Configuration;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(
    private httpClient: HttpClient,
    private principleService: PrincipleService,
    private storageService: SessionStorageService
  ) { 
    this.unsubscribe$ = new ReplaySubject(1);

    this.configurations$.pipe(
      takeUntil(this.unsubscribe$),
      tap(x => {
        console.log(x);
        if (x !== undefined && x !== null) {
          this.configuration = new Configuration({
            accessToken: this.storageService.retrieve(
              'authorizationData_wmsspa'
            ),
            basePath: x.warehouseUrl
          });

          this.principleService = new PrincipleService(
            this.httpClient,
            x.warehouseUrl,
            this.configuration
          );
        }
      })
    ).subscribe();
  }

  @Selector()
  static principles(state: PrincipleStateModel) {
    return state.principles;
  }
  @Selector()
  static selectedPrinciple(state: PrincipleStateModel) {
    return state.selectedPrinciple;
  }
  @Selector()
  static principleItems(state: PrincipleStateModel) {
    return state.principleItems;
  }

@Action(AddPrinciple)
addMaterial({ dispatch }: StateContext<PrincipleStateModel>, { payload }: AddPrinciple) {

  dispatch(RegisterLoadingOverlay);

  return this.principleService.apiV1PrincipleNewPost(payload,Guid.newGuid()).pipe(
    tap(
      result => console.log("add principle"),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay, new Navigate({
          commands: ['/principle/list']
        })]);
      }
    )
  );
}
@Action(EditPrinciple)
editPrinciple({ dispatch, patchState }: StateContext<PrincipleStateModel>, { payload }: EditPrinciple) {

  dispatch(RegisterLoadingOverlay);

  return this.principleService.apiV1PrincipleEditPut(payload,Guid.newGuid()).pipe(
    tap(
      result => console.log("edit principle"),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        patchState({
        selectedPrinciple: null
      });
        dispatch([ResolveLoadingOverlay, new Navigate({
          commands: ['/principle/list']
        })]);
      }
    )
  );
}
@Action(DeletePrinciple)
deletePrinciple({ dispatch }: StateContext<PrincipleStateModel>, { payload }: DeletePrinciple) {

  dispatch(RegisterLoadingOverlay);

  // return this.principleService.apiV1PrincipleDelete(payload.id).pipe(
  //   tap(
  //     result => {
  //       if (!result) new Alert(err.error);
  //     },
  //     (err: HttpErrorResponse) =>
  //       dispatch([
  //         new Alert(err.error),
  //         ResolveLoadingOverlay
  //       ]),
  //     () => {
  //       dispatch([ResolveLoadingOverlay, new FetchPrincipleByUser(payload.ownerId)]);
  //     }
  //   )
  // );
}

@Action(FetchPrinciple)
fetchPrinciple({ dispatch,patchState }: StateContext<PrincipleStateModel>) {

  dispatch(RegisterLoadingOverlay);

  return this.principleService.apiV1PrincipleGet().pipe(
    tap(
      result => patchState({
        principles: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay]);
      }
    )
  );
}

@Action(FetchPrincipleById)
fetchPrincipleById({ dispatch,patchState }: StateContext<PrincipleStateModel>, 
  { payload }: FetchPrincipleById) {

  dispatch(RegisterLoadingOverlay);

  return this.principleService.apiV1PrincipleIdGet(payload).pipe(
    tap(
      result => patchState({
        selectedPrinciple: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay]);
      }
    )
  );
}

@Action(FetchPrincipleInventoryById)
fetchPrincipleInventoryById({ dispatch,patchState }: StateContext<PrincipleStateModel>, 
  { payload }: FetchPrincipleInventoryById) {
  dispatch(RegisterLoadingOverlay);

  return this.principleService.apiV1PrincipleItemsIdGet(payload).pipe(
    tap(
      result => patchState({
        principleItems: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay]);
      }
    )
  );
}
}
