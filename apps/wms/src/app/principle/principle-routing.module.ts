import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipleAddComponent } from './principle-add/principle-add.component';
import { PrincipleDashboardTableComponent } from './principle-dashboard-table/principle-dashboard-table.component';
import { PrincipleDashboardComponent } from './principle-dashboard/principle-dashboard.component';
import { PrincipleEditComponent } from './principle-edit/principle-edit.component';
import { PrincipleListComponent } from './principle-list/principle-list.component';

const routes: Routes = [
  { path: '', component: PrincipleListComponent },
  { path: 'dashboard', component: PrincipleDashboardComponent },
  { path: 'dashboard/table/:id', component: PrincipleDashboardTableComponent },
  { path: 'list', component: PrincipleListComponent },
  { path: 'add', component: PrincipleAddComponent },
  { path: 'edit/:id', component: PrincipleEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrincipleRoutingModule { }
