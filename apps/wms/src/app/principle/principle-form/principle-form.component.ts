import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Alert } from '@fluctware/core';
import { Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PrincipleViewModel } from '../../services/warehouse-api';

@Component({
  selector: 'fluctware-principle-form',
  templateUrl: './principle-form.component.html',
  styleUrls: ['./principle-form.component.scss']
})
export class PrincipleFormComponent implements OnInit , OnDestroy{

  @Input()
  userId: string;

  @Input()
  title: string;

  @Input()
  principle$: Observable<PrincipleViewModel>;

  @Output()
  onSubmit = new EventEmitter<PrincipleViewModel>();

  principleForm: FormGroup;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private fb: FormBuilder, private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
    this.principleForm = this.fb.group({
      id: [""],
      name: ["", [Validators.required]]
    });
  }

  ngOnInit() {
    this.principle$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(principle => {
        this.principleForm.patchValue(
          {
            id: principle?.id,
            name: principle?.name
          });
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  submit() {
    if (!this.principleForm.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      return;
    }
    var wh: PrincipleViewModel = {
      id: this.principleForm.value.id,
      name: this.principleForm.value.name
    };
    this.onSubmit.emit(wh);
  }
}
