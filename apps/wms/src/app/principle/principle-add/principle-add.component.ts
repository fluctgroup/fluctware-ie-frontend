import { Component, OnInit } from '@angular/core';
import { CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { PrincipleViewModel } from '../../services/warehouse-api';

import { AddPrinciple } from '../state/principle.actions';

@Component({
  selector: 'fluctware-principle-add',
  templateUrl: './principle-add.component.html',
  styleUrls: ['./principle-add.component.scss']
})
export class PrincipleAddComponent implements OnInit {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  constructor(private store: Store) { }

  ngOnInit() {
  }
  submit(principle:PrincipleViewModel){
    this.store.dispatch(new AddPrinciple(principle));
  }
}
