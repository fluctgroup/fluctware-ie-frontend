import { Component, OnDestroy, OnInit } from '@angular/core';
import { IConfirmConfig } from '@covalent/core/dialogs';
import { Confirm, CoreState, Navigate } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PrincipleViewModel } from '../../services/warehouse-api';
import { FetchPrinciple } from '../state/principle.actions';
import { PrincipleState } from '../state/principle.state';

@Component({
  selector: 'app-principle-list',
  templateUrl: './principle-list.component.html',
  styleUrls: ['./principle-list.component.scss']
})
export class PrincipleListComponent implements OnInit,OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(PrincipleState.principles)
  principles$: Observable<PrincipleViewModel[]>;

  unsubscribe$: ReplaySubject<boolean>;
  
  constructor(private store: Store) { 
    this.unsubscribe$ = new ReplaySubject(1);
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }

  ngOnInit() {
    this.userData$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        this.store.dispatch(FetchPrinciple);
      });
  }
  editPrinciple(element:PrincipleViewModel){
    this.store.dispatch(new Navigate({ commands: [ '/principle/edit', element.id] }));
  }
  // deleteWarehouse(element:PrincipleViewModel){
  //   var config=<IConfirmConfig>{
  //     acceptButton:'Confirm',
  //     cancelButton:'Back',
  //     title:'Are you sure?',
  //     message:'this action cannot be undone'
  //   };
  //   var dialog=new Confirm({
  //     config:config,
  //     onAccept:()=>this.store.dispatch(new DeleteWarehouse(element)),
  //     onReject: ()=>console.log('do nothing')
  //   });
  //   this.store.dispatch(dialog);
    
  // }
}
