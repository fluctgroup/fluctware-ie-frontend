import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PrincipleInventoryViewModel } from '../../services/warehouse-api';
import { FetchPrincipleInventoryById } from '../state/principle.actions';
import { PrincipleState } from '../state/principle.state';

@Component({
  selector: 'fluctware-principle-dashboard-table',
  templateUrl: './principle-dashboard-table.component.html',
  styleUrls: ['./principle-dashboard-table.component.scss']
})
export class PrincipleDashboardTableComponent implements AfterViewInit, OnInit,OnDestroy {
  displayedColumns: string[] = [
    "warehouseName",
    "name",
    "expiryDate",
    "qty",
    "quantifier",
    "sku",
    "areaLevel1",
    "areaLevel2",
    "areaLevel3",
    "areaLevel4",
  ];
  dataSource: MatTableDataSource<PrincipleInventoryViewModel>;

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  @Select(PrincipleState.principleItems)
  principleInventory$: Observable<PrincipleInventoryViewModel[]>;

  itemId: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  unsubscribe$: ReplaySubject<boolean>;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  constructor(private route: ActivatedRoute, private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }

  ngOnInit() {
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe((y) => {
      if (y) {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.itemId = Number(params.get("id"));
          this.store.dispatch(new FetchPrincipleInventoryById(this.itemId));
        });
        this.principleInventory$
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe((transfer) => {
            this.dataSource = new MatTableDataSource(transfer);
          });
      }
    });
  }
}
