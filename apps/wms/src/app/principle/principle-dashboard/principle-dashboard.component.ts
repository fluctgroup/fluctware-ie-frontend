import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Alert, CoreState, Navigate } from "@fluctware/core";
import { Select, Store } from "@ngxs/store";
import { Observable, ReplaySubject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { PrincipleViewModel } from "../../services/warehouse-api";
import { FetchPrinciple } from "../state/principle.actions";
import { PrincipleState } from "../state/principle.state";

@Component({
  selector: "fluctware-principle-dashboard",
  templateUrl: "./principle-dashboard.component.html",
  styleUrls: ["./principle-dashboard.component.scss"],
})
export class PrincipleDashboardComponent implements OnInit, OnDestroy {
  
  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(PrincipleState.principles)
  principles$: Observable<PrincipleViewModel[]>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  title: string;

  principleForm: FormGroup;
  principleId: number;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private fb: FormBuilder, private store: Store) {
    this.title = "Query principle items";
    this.unsubscribe$ = new ReplaySubject(1);
    this.principleForm = this.fb.group({
      id: [""],
      name: ["", [Validators.required]]
    });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(false);
  }

  ngOnInit() {
    this.userData$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        this.store.dispatch(FetchPrinciple);
      });
  }
  
  onPrincipleChange(principle: any) {
    
    if (principle.value!=="") {
      this.store.dispatch(new Navigate({
        commands: ['/principle/dashboard/table',this.principleForm.value.id]
      }));
      return;
    }
  }
}
