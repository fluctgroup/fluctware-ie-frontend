import { EditStockCommand ,CheckWarehouseStockViewModel, CheckStockByExpireViewModel} from "../../services/warehouse-api";

export class EditInventory {
  static readonly type = '[WMS] EDIT INVENTORY';
  constructor(public payload: EditStockCommand) { }
}

export class FetchInventoryByStockId {
  static readonly type = '[WMS] FETCH INVENTORY BY STOCK ID';
  constructor(public payload: number) { }
}

export class FetchInventoryHistoryByStockId {
  static readonly type = '[WMS] FETCH INVENTORY HISTORY BY STOCK ID';
  constructor(public payload: number) { }
}

export class FetchInventoriesByWarehouseId {
  static readonly type = '[WMS] FETCH INVENTORIES BY WAREHOUSE ID';
  constructor(public payload: number) { }
}

export class FetchAllStockWarehouseByExpiry {
  static readonly type = '[WMS] FETCH ALL STOCK WAREHOUSE BY EXPIRY';
  constructor(public payload: CheckStockByExpireViewModel) { }
}
export class FetchAllStockWarehouse {
  static readonly type = '[WMS] FETCH ALL STOCK WAREHOUSE';
  constructor(public payload: CheckWarehouseStockViewModel) { }
}
export class ClearInventoryStateData {
  static readonly type = '[WMS] CLEAR INVENTORY STATE DATA';
  constructor() { }
}