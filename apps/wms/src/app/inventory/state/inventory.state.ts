import { State, Selector, StateContext, Action, Select } from '@ngxs/store';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CoreState, IConfiguration, RegisterLoadingOverlay, Alert, ResolveLoadingOverlay, Navigate, SessionStorageService, Guid } from '@fluctware/core';
import { Observable, ReplaySubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { tap, takeUntil } from 'rxjs/operators';
import { Configuration, InventoryHistoryViewModel, InventoryService, InventoryViewModel, StockExpiryPairViewModel, WarehouseStockPairViewModel } from '../../services/warehouse-api';
import { ClearInventoryStateData, EditInventory, FetchAllStockWarehouse, FetchAllStockWarehouseByExpiry, FetchInventoriesByWarehouseId, FetchInventoryByStockId, FetchInventoryHistoryByStockId } from './inventory.actions';

export interface InventoryStateModel {
  selectedInventory: InventoryViewModel;
  inventories: InventoryViewModel[];
  modifiedLogs: InventoryHistoryViewModel[];
  warehouseInventory: StockExpiryPairViewModel[];
  warehouseInventories: WarehouseStockPairViewModel[];
}
const defaults: InventoryStateModel = {
  selectedInventory: null,
  inventories: [],
  modifiedLogs:[],
  warehouseInventory: null,
  warehouseInventories: []
};
@State<InventoryStateModel>({
  name: 'Inventory',
  defaults: defaults
})
@Injectable()
export class InventoryState {

  @Select(CoreState.configuration)
  configurations$: Observable<IConfiguration>;

  configuration: Configuration;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(
    private httpClient: HttpClient,
    private inventoryService: InventoryService,
    private storageService: SessionStorageService
  ) {
    this.unsubscribe$ = new ReplaySubject(1);

    this.configurations$.pipe(
      takeUntil(this.unsubscribe$),
      tap(x => {
        console.log(x);
        if (x !== undefined && x !== null) {
          this.configuration = new Configuration({
            accessToken: this.storageService.retrieve(
              'authorizationData_wmsspa'
            ),
            basePath: x.warehouseUrl
          });

          this.inventoryService = new InventoryService(
            this.httpClient,
            x.warehouseUrl,
            this.configuration
          );
        }
      })
    ).subscribe();
  }

  @Selector()
  static selectedInventory(state: InventoryStateModel) {
    return state.selectedInventory;
  }
  @Selector()
  static inventories(state: InventoryStateModel) {
    return state.inventories;
  }

  @Selector()
  static modifiedLogs(state: InventoryStateModel) {
    return state.modifiedLogs;
  }

  @Selector()
  static warehouseInventory(state: InventoryStateModel) {
    return state.warehouseInventory;
  }

  @Selector()
  static warehouseInventories(state: InventoryStateModel) {
    return state.warehouseInventories;
  }

  @Action(EditInventory)
  editInventory({ dispatch, patchState }: StateContext<InventoryStateModel>, { payload }: EditInventory) {

    dispatch(RegisterLoadingOverlay);

    return this.inventoryService.apiInventoryEditPut(payload, Guid.newGuid()).pipe(
      tap(
        result => console.log("edit inventory"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          patchState({
            selectedInventory: null
          });
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/inventory/list']
          })]);
        }
      )
    );
  }


  @Action(FetchInventoriesByWarehouseId)
  fetchInventoriesByWarehouseId({ dispatch, patchState }: StateContext<InventoryStateModel>, { payload }: FetchInventoriesByWarehouseId) {

    dispatch(RegisterLoadingOverlay);

    return this.inventoryService.apiInventoryWarehouseIdGet(payload).pipe(
      tap(
        result => patchState({
          inventories: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }

  @Action(FetchInventoryHistoryByStockId)
  fetchInventoryHistoryByStockId({ dispatch, patchState }: StateContext<InventoryStateModel>, { payload }: FetchInventoryHistoryByStockId) {

    dispatch(RegisterLoadingOverlay);

    return this.inventoryService.apiInventoryStockHistoryIdGet(payload).pipe(
      tap(
        result => patchState({
          modifiedLogs: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }


  @Action(FetchInventoryByStockId)
  fetchInventoryByStockId({ dispatch, patchState }: StateContext<InventoryStateModel>, { payload }: FetchInventoryByStockId) {

    dispatch(RegisterLoadingOverlay);

    return this.inventoryService.apiInventoryStockIdGet(payload).pipe(
      tap(
        result => patchState({
          selectedInventory: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }

  @Action(FetchAllStockWarehouse)
  fetchAllStockWarehouse({ dispatch, patchState }: StateContext<InventoryStateModel>, { payload }: FetchAllStockWarehouse) {

    dispatch(RegisterLoadingOverlay);

    return this.inventoryService.apiInventoryCheckStockWarehousePost(payload).pipe(
      tap(
        result => patchState({
          warehouseInventory: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay,
            new Navigate({commands: ['/transfer']})
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }

  @Action(FetchAllStockWarehouseByExpiry)
  fetchAllStockWarehouseByExpiry({ dispatch, patchState }: StateContext<InventoryStateModel>, { payload }: FetchAllStockWarehouseByExpiry) {

    dispatch(RegisterLoadingOverlay);

    return this.inventoryService.apiInventoryCheckStockByExpirePost(payload).pipe(
      tap(
        result => patchState({
          warehouseInventories: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }

  @Action(ClearInventoryStateData)
  clearInventoryStateData({ patchState }: StateContext<InventoryStateModel>) {
    patchState({
      selectedInventory: null,
      inventories: [],
      warehouseInventories: [],
      warehouseInventory:null
    })
  }
}
