import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { InventoryHistoryViewModel } from '../../services/warehouse-api';
import { FetchInventoryHistoryByStockId } from '../state/inventory.actions';
import { InventoryState } from '../state/inventory.state';

@Component({
  selector: 'app-inventory-log',
  templateUrl: './inventory-log.component.html',
  styleUrls: ['./inventory-log.component.scss']
})
export class InventoryLogComponent implements OnInit {
  displayedColumns: string[] = ['modifiedDate','modifiedBy', 'productName','beforeStock','afterStock'];
  dataSource: MatTableDataSource<InventoryHistoryViewModel>;

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  @Select(InventoryState.modifiedLogs)
  logs$: Observable<InventoryHistoryViewModel[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private route: ActivatedRoute,private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  ngOnInit(): void {
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
      if (y) {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.store.dispatch(new FetchInventoryHistoryByStockId(Number(params.get('id'))));
      
        });
      }
    });
    this.userData$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if (user) {
          // this.store.dispatch(new FetchWarehouseByUser(user.sub));
        }
      })
    
    this.logs$.pipe(takeUntil(this.unsubscribe$))
    .subscribe(logs => {
      this.dataSource = new MatTableDataSource(logs);
    });
    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
