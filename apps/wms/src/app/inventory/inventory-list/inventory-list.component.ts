import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CoreState, Navigate } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { InventoryViewModel, Owner, WarehouseViewModel } from '../../services/warehouse-api';
import { FetchOwnerById, FetchWarehouseByUser } from '../../warehouse/state/warehouse.actions';
import { WarehouseState } from '../../warehouse/state/warehouse.state';
import { FetchInventoriesByWarehouseId } from '../state/inventory.actions';
import { InventoryState } from '../state/inventory.state';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss']
})
export class InventoryListComponent implements OnInit {
  displayedColumns: string[] = ['name','skuPrinciple','qty','expiryDate','actions'];
  dataSource: MatTableDataSource<InventoryViewModel>;

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(InventoryState.inventories)
  inventories$: Observable<InventoryViewModel[]>;

  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  unsubscribe$: ReplaySubject<boolean>;

  constructor(private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(false);
  }

  ngOnInit() {
    this.userData$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        if (vals !== null) {
          this.store.dispatch(new FetchWarehouseByUser(vals.sub));
        }
      });
    this.warehouse$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        if (vals !== null) {
          this.store.dispatch(new FetchInventoriesByWarehouseId(vals.id));
        }
      });
      this.inventories$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        this.dataSource = new MatTableDataSource(vals);
      });
  }
  editInventory(element: InventoryViewModel) {
    this.store.dispatch(new Navigate({ commands: ['inventory/edit', element.id] }));
  }
  historyInventory(element: InventoryViewModel) {
    this.store.dispatch(new Navigate({ commands: ['inventory/history', element.id] }));
  }
  return(element: InventoryViewModel) {
    this.store.dispatch(new Navigate({ commands: ['return', element.sku] }));
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
