import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryFormComponent } from './inventory-form/inventory-form.component';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { InventoryLogComponent } from './inventory-log/inventory-log.component';

const routes: Routes = [
  { path: '', component: InventoryListComponent },
  { path: 'edit/:id', component: InventoryFormComponent },
  { path: 'list', component: InventoryListComponent },
  { path: 'history/:id', component: InventoryLogComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }
