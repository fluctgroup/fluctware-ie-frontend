import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TransferState } from '@angular/platform-browser';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Alert, CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FetchPrincipleById } from '../../principle/state/principle.actions';
import { PrincipleState } from '../../principle/state/principle.state';
import { EditStockCommand, InventoryViewModel, PrincipleViewModel } from '../../services/warehouse-api';
import { EditInventory, FetchInventoryByStockId } from '../state/inventory.actions';
import { InventoryState } from '../state/inventory.state';

@Component({
  selector: 'app-inventory-form',
  templateUrl: './inventory-form.component.html',
  styleUrls: ['./inventory-form.component.scss']
})
export class InventoryFormComponent implements OnInit, OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  itemId: number;
  userId:string;
  title:string;

  
  @Select(PrincipleState.selectedPrinciple)
  principle$:Observable<PrincipleViewModel>;

  @Select(InventoryState.selectedInventory)
  inventory$: Observable<InventoryViewModel>;

  inventoryForm: FormGroup;
  unsubscribe$: ReplaySubject<boolean>;
  constructor(private route: ActivatedRoute,private fb: FormBuilder, 
    private store: Store) {
    this.title="Edit Inventory";
    this.unsubscribe$ = new ReplaySubject(1);
    this.inventoryForm = this.fb.group({
      stockId: [0, [Validators.required]],
      expiryDate: [0],
      inboundDate: [0, [Validators.required]],
      qty: [0, [Validators.required]],
      note: [0],
      sku: [0, [Validators.required]],
      authorId: [0, [Validators.required]],
      quantifier: [0, [Validators.required]],
      unique: [0, [Validators.required]]
    });
  }

  ngOnInit() {
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
      if (y) {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.itemId = Number(params.get('id'));
          this.store.dispatch(new FetchInventoryByStockId(this.itemId));
          this.inventoryForm.patchValue(
            {
              stockId: this.itemId
            });
        });
      }
    });
    this.userData$?.pipe(takeUntil(this.unsubscribe$))
    .subscribe(user=>{
      if (user) {
        this.userId=user.sub;
      }
    });
    this.inventory$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(inventory => {
        if(inventory!==null){
          this.store.dispatch(new FetchPrincipleById(inventory.product.principleId));
          this.inventoryForm.patchValue(
            {
              expiryDate: inventory?.expiryDate,
              inboundDate: inventory?.inboundDate,
              qty: inventory?.qty,
              note: inventory?.note,
              sku: inventory?.sku,
              authorId: inventory?.authorId,
              quantifier: inventory?.quantifier,
              unique: inventory?.unique,
            });  
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  
  submit() {
    if (!this.inventoryForm.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      return;
    }
    var wh: EditStockCommand = {
      stockId: +this.inventoryForm.value.stockId,
      expiryDate: this.inventoryForm.value.expiryDate,
      qty: this.inventoryForm.value.qty,
      note: this.inventoryForm.value.note,
      sku: this.inventoryForm.value.sku,
      authorId: this.inventoryForm.value.authorId,
      quantifier: this.inventoryForm.value.quantifier,
      unique: this.inventoryForm.value.unique
    };
    this.store.dispatch(new EditInventory(wh));
  }
}
