import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fluctware/shared';
import { ApiModule } from '../services/warehouse-api';
import { InventoryFormComponent } from './inventory-form/inventory-form.component';
import { InventoryRoutingModule } from './inventory-routing.module';
import { InventoryState } from './state/inventory.state';
import { NgxsModule } from '@ngxs/store';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { PrincipleState } from '../principle/state/principle.state';
import { InventoryLogComponent } from './inventory-log/inventory-log.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ApiModule,
    NgxsModule.forFeature([PrincipleState,InventoryState]),
    InventoryRoutingModule,
  ],
  declarations: [
    InventoryFormComponent,
    InventoryListComponent,
    InventoryLogComponent
  ]
})
export class InventoryModule { }
