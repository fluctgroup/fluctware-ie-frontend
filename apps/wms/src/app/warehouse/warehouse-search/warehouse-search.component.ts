import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Alert, Navigate, RegisterLoadingOverlay, ResolveLoadingOverlay } from '@fluctware/core';
import { Store } from '@ngxs/store';
import { ReplaySubject } from 'rxjs';
import { SearchWarehouseViewModel } from '../../services/warehouse-api';
import { SearchWarehouseByArea } from '../state/warehouse.actions';

import PlaceResult = google.maps.places.PlaceResult;

@Component({
  selector: 'fluctware-warehouse-search',
  templateUrl: './warehouse-search.component.html',
  styleUrls: ['./warehouse-search.component.scss']
})
export class WarehouseSearchComponent implements OnInit,OnDestroy {

  whSearchForm:FormGroup;
  formattedaddress:string;
  
  unsubscribe$: ReplaySubject<boolean>;
  constructor(private fb: FormBuilder, private store:Store) {
    this.unsubscribe$ = new ReplaySubject(1);
    const today = new Date();
    const date = today.getDate();
    const month = today.getMonth();
    const year = today.getFullYear();
    this.whSearchForm=this.fb.group({
      areaLevel1: ["",[Validators.required]],
      areaLevel2: [""],
      areaLevel3: [""],
      space: [10],
      startDate: [new Date(year, month,date),[Validators.required]],
      endDate: [new Date(year, month,date+7),[Validators.required]],
    });
   }

  ngOnInit() {
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  findAreaLevel(key:string,geocode:google.maps.GeocoderAddressComponent){
    return geocode.types.find(x=>x===key);
  }
  onAutocompleteSelected(result: PlaceResult) {
    this.store.dispatch(RegisterLoadingOverlay); 
    this.whSearchForm.patchValue(
      {
        areaLevel1:result.address_components.find(x=>this.findAreaLevel("administrative_area_level_1",x))?.long_name,
        areaLevel2:result.address_components.find(x=>this.findAreaLevel("administrative_area_level_2",x))?.long_name,
        areaLevel3:result.address_components.find(x=>this.findAreaLevel("administrative_area_level_3",x))?.long_name,
      });
    console.log(this.whSearchForm.value);
    this.store.dispatch(ResolveLoadingOverlay);
  } 
  onLocationSelected(location: Location) {
    this.store.dispatch(RegisterLoadingOverlay);
    console.log('onLocationSelected: ', location);
    this.store.dispatch(ResolveLoadingOverlay);
  }
  search(){
    if (!this.whSearchForm.valid) {
      if(this.whSearchForm.value.areaLevel1===undefined){
        this.store.dispatch(new Alert("Alamat belum terpilih"));
      }else{
        this.store.dispatch(new Alert("Form belum lengkap"));
      }
      return;
    }
    var wh:SearchWarehouseViewModel={
      spaceRequired: this.whSearchForm.value.space,
      startDate: this.whSearchForm.value.startDate,
      endDate:this.whSearchForm.value.endDate,
      areaLevel1:this.whSearchForm.value.areaLevel1,
      areaLevel2:this.whSearchForm.value.areaLevel2,
      areaLevel3:this.whSearchForm.value.areaLevel3
    };
    this.store.dispatch(new SearchWarehouseByArea(wh));
  
  }
}
