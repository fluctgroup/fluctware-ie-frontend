import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WarehouseFormComponent } from './warehouse-form/warehouse-form.component';
import { SharedModule } from '@fluctware/shared';
import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

import { AgmCoreModule } from '@agm/core';

import { WarehouseRoutingModule } from './warehouse-routing.module';
import { WarehouseSearchComponent } from './warehouse-search/warehouse-search.component';
import { WarehouseBookComponent } from './warehouse-book/warehouse-book.component';
import { WarehouseListComponent } from './warehouse-list/warehouse-list.component';
import { WarehouseAddComponent } from './warehouse-add/warehouse-add.component';
import { WarehouseEditComponent } from './warehouse-edit/warehouse-edit.component';
import { NgxsModule } from '@ngxs/store';
import { WarehouseState } from './state/warehouse.state';
import { BookDialogComponent } from './warehouse-book/book-dialog/book-dialog.component';
import { ApiModule } from '../services/warehouse-api';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ApiModule,
    NgxsModule.forFeature([WarehouseState]),
    MatGoogleMapsAutocompleteModule,
    NgxMaterialTimepickerModule,
    AgmCoreModule,
    WarehouseRoutingModule,
  ],
  declarations: [WarehouseFormComponent,
    WarehouseSearchComponent,
    WarehouseBookComponent,
    WarehouseListComponent,
    WarehouseAddComponent,
    WarehouseEditComponent,
    BookDialogComponent]
})
export class WarehouseModule { }
