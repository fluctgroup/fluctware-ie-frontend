import { Component, OnDestroy, OnInit } from '@angular/core';
import { IConfirmConfig } from '@covalent/core/dialogs';
import { Alert, Confirm, CoreState, Navigate } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { WarehouseElementViewModel } from '../../services/warehouse-api';
import { DeleteWarehouse, FetchWarehouseById, FetchWarehouseByUser, FetchWarehouses } from '../state/warehouse.actions';
import { WarehouseState } from '../state/warehouse.state';

@Component({
  selector: 'fluctware-warehouse-list',
  templateUrl: './warehouse-list.component.html',
  styleUrls: ['./warehouse-list.component.scss']
})
export class WarehouseListComponent implements OnInit,OnDestroy {
  
  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(WarehouseState.warehouses)
  warehouses$: Observable<WarehouseElementViewModel[]>;

  unsubscribe$: ReplaySubject<boolean>;
  
  constructor(private store: Store) { 
    this.unsubscribe$ = new ReplaySubject(1);
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(false);
  }

  ngOnInit() {
    this.userData$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        this.store.dispatch(new FetchWarehouses());
      });
  }
  editWarehouse(element:WarehouseElementViewModel){
    this.store.dispatch(new Navigate({ commands: [ '/warehouse/edit', element.id] }));
  }
  addWarehouse(){
    this.store.dispatch(new Navigate({ commands: [ '/warehouse/add'] }));
  }
  deleteWarehouse(element:WarehouseElementViewModel){
    var config=<IConfirmConfig>{
      acceptButton:'Confirm',
      cancelButton:'Back',
      title:'Are you sure?',
      message:'this action cannot be undone'
    };
    var dialog=new Confirm({
      config:config,
      onAccept:()=>this.store.dispatch(new DeleteWarehouse(element)),
      onReject: ()=>console.log('do nothing')
    });
    this.store.dispatch(dialog);
    
  }
}
