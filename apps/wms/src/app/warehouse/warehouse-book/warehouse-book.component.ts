import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CoreState, Navigate, OpenCustomDialog } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { WarehouseDetailViewModel } from '../../services/warehouse-api';
import { WarehouseState } from '../state/warehouse.state';
import { BookDialogComponent } from './book-dialog/book-dialog.component';

@Component({
  selector: 'fluctware-warehouse-book',
  templateUrl: './warehouse-book.component.html',
  styleUrls: ['./warehouse-book.component.scss']
})
export class WarehouseBookComponent implements OnInit,OnDestroy {
  @Select(WarehouseState.selectedWarehouseDetail)
  warehouseDetail$: Observable<WarehouseDetailViewModel>;
  
  whBookForm:FormGroup;
  unsubscribe$: ReplaySubject<boolean>;
  pricePerUnit:number=100000;
  totalPrice:number;

  constructor(private fb: FormBuilder,private store:Store) {
    this.unsubscribe$ = new ReplaySubject(1);
    this.whBookForm= this.fb.group({
      space: [1]
    });
  }


  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  ngOnInit() {
    this.warehouseDetail$?.pipe(takeUntil(this.unsubscribe$))
    .subscribe(warehouse => {
      this.whBookForm.patchValue(
        {
          space: warehouse?.spaceRequired,
        });
      this.totalPrice=warehouse?.spaceRequired * this.pricePerUnit;
    });
  }
  spaceChanged(){
    this.totalPrice=this.whBookForm.value.space * this.pricePerUnit;
    
  }
  book(){
    this.store.dispatch(new OpenCustomDialog({
      content: BookDialogComponent
    }))
  }
}
