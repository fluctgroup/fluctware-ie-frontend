import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Owner, UpdateWarehouseCommand, WarehouseViewModel } from '../../services/warehouse-api';
import { EditWarehouse, FetchAvailableOwners, FetchWarehouseById } from '../state/warehouse.actions';
import { WarehouseState } from '../state/warehouse.state';

@Component({
  selector: 'fluctware-warehouse-edit',
  templateUrl: './warehouse-edit.component.html',
  styleUrls: ['./warehouse-edit.component.scss']
})
export class WarehouseEditComponent implements OnInit,OnDestroy {
  
  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;
  
  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(WarehouseState.availableOwners)
  owners$:Observable<Owner[]>;
  
  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  itemId: number;
  
  unsubscribe$: ReplaySubject<boolean>;

  constructor(private route: ActivatedRoute, private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  ngOnInit() {
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
      if (y) {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.itemId = Number(params.get('id'));
          this.store.dispatch([new FetchWarehouseById(this.itemId),FetchAvailableOwners]);
        });
      }
    })
  }
  submit(warehouse:WarehouseViewModel){
    var update:UpdateWarehouseCommand={
      warehouseId:warehouse.id,
      name:warehouse.name,
      spaceLeft:warehouse.spaceLeft,
      latitude:warehouse.latitude,
      longitude:warehouse.longitude,
      opens: warehouse.opens,
      closed:warehouse.closed,
      ownerId:warehouse.ownerId,
      areaLevel1:warehouse.areaLevel1,
      areaLevel2:warehouse.areaLevel2,
      areaLevel3:warehouse.areaLevel3,
      areaLevel4:warehouse.areaLevel4,
      freezers:warehouse.freezer,
      chillers:warehouse.chiller
    }
    this.store.dispatch(new EditWarehouse(update));
  }
}
