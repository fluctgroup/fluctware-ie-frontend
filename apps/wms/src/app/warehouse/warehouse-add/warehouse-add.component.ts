import { Component, OnInit } from '@angular/core';
import { CoreState, Navigate } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AddWarehouseCommand, Owner, WarehouseViewModel } from '../../services/warehouse-api';
import { AddWarehouse, FetchAvailableOwners } from '../state/warehouse.actions';
import { WarehouseState } from '../state/warehouse.state';

@Component({
  selector: 'fluctware-warehouse-add',
  templateUrl: './warehouse-add.component.html',
  styleUrls: ['./warehouse-add.component.scss']
})
export class WarehouseAddComponent implements OnInit {
  @Select(WarehouseState.selectedWarehouse)
  warehouse$:Observable<WarehouseViewModel>;
  
  @Select(WarehouseState.availableOwners)
  owners$:Observable<Owner[]>;
  
  @Select(CoreState.userData)
  userData$: Observable<any>;

  constructor(private store: Store) {
  }

  ngOnInit() {
    this.store.dispatch([FetchAvailableOwners]);
  }
  submit(warehouse:WarehouseViewModel){
    var cmd:AddWarehouseCommand={
      name:warehouse.name,
      spaceLeft:warehouse.spaceLeft,
      latitude:warehouse.latitude,
      longitude:warehouse.longitude,
      opens: warehouse.opens,
      closed:warehouse.closed,
      ownerId:warehouse.ownerId,
      areaLevel1:warehouse.areaLevel1,
      areaLevel2:warehouse.areaLevel2,
      areaLevel3:warehouse.areaLevel3,
      areaLevel4:warehouse.areaLevel4,
      freezer:warehouse.freezer,
      chiller:warehouse.chiller
    }
    this.store.dispatch(new AddWarehouse(cmd));
  }
  back() {
    this.store.dispatch(new Navigate({
      commands: ['list']
    }));
  }
}
