import { WarehouseViewModel, SearchWarehouseViewModel, UpdateWarehouseCommand, AddWarehouseCommand } from "../../services/warehouse-api";

export class AddWarehouse {
  static readonly type = '[WMS] ADD WAREHOUSE';
  constructor(public payload: AddWarehouseCommand) { }
}
export class EditWarehouse {
  static readonly type = '[WMS] EDIT WAREHOUSE';
  constructor(public payload: UpdateWarehouseCommand) { }
}

export class DeleteWarehouse {
  static readonly type = '[WMS] DELETE WAREHOUSE';
  constructor(public payload: WarehouseViewModel) { }
}

export class FetchWarehouses {
  static readonly type = '[WMS] FETCH WAREHOUSES';
  constructor() { }
}
export class FetchWarehousesExcept {
  static readonly type = '[WMS] FETCH WAREHOUSES EXCEPT';
  constructor(public payload: number) { }
}
export class FetchWarehouseByUser {
  static readonly type = '[WMS] FETCH WAREHOUSE BY USER';
  constructor(public payload: string) { }
}
export class FetchWarehouseById {
  static readonly type = '[WMS] FETCH WAREHOUSE BY ID';
  constructor(public payload: number) { }
}
export class SearchWarehouseByArea {
  static readonly type = '[WMS] SEARCH WAREHOUSE BY AREA';
  constructor(public payload: SearchWarehouseViewModel) { }
}

export class FetchOwnerById {
  static readonly type = '[WMS] FETCH OWNER BY ID';
  constructor(public payload: string) { }
}

export class FetchAvailableOwners {
  static readonly type = '[WMS] FETCH AVAILABLE OWNERS';
  constructor() { }
}