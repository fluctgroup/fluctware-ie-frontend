import { State, Selector, StateContext, Action, Select } from '@ngxs/store';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CoreState, IConfiguration, RegisterLoadingOverlay, Alert, ResolveLoadingOverlay, Navigate, SessionStorageService, Guid } from '@fluctware/core';
import { Observable, ReplaySubject } from 'rxjs';

import { AddWarehouse, DeleteWarehouse, EditWarehouse, FetchAvailableOwners, FetchOwnerById, FetchWarehouseById, FetchWarehouseByUser, FetchWarehouses, FetchWarehousesExcept, SearchWarehouseByArea} from './warehouse.actions';
import { Injectable } from '@angular/core';
import { tap, takeUntil } from 'rxjs/operators';
import { Configuration, Owner, WarehouseDetailViewModel, WarehouseElementViewModel, WarehouseService, WarehouseViewModel } from '../../services/warehouse-api';
import { UserViewModel } from '../../services/identity-api';

export interface WarehouseStateModel {
  users:UserViewModel[];
  availableOwners:Owner[];
  selectedOwner:Owner;
  warehouses: WarehouseElementViewModel[];
  selectedWarehouse:WarehouseViewModel;
  selectedWarehouseDetail:WarehouseDetailViewModel;
}
const defaults: WarehouseStateModel = {
  users:[],
  availableOwners:[],
  selectedOwner:null,
  warehouses:[],
  selectedWarehouse:null,
  selectedWarehouseDetail:null,
};
@State<WarehouseStateModel>({
  name: 'Warehouse',
  defaults: defaults
})
@Injectable()
export class WarehouseState {

  @Select(CoreState.configuration) 
  configurations$: Observable<IConfiguration>;

  configuration: Configuration;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(
    private httpClient: HttpClient,
    private warehouseService: WarehouseService,
    private storageService: SessionStorageService
  ) { 
    this.unsubscribe$ = new ReplaySubject(1);

    this.configurations$.pipe(
      takeUntil(this.unsubscribe$),
      tap(x => {
        console.log(x);
        if (x !== undefined && x !== null) {
          this.configuration = new Configuration({
            accessToken: this.storageService.retrieve(
              'authorizationData_wmsspa'
            ),
            basePath: x.warehouseUrl
          });
          this.warehouseService = new WarehouseService(
            this.httpClient,
            x.warehouseUrl,
            this.configuration
          );
        }
      })
    ).subscribe();
  }

  @Selector()
  static warehouses(state: WarehouseStateModel) {
    return state.warehouses;
  }
  @Selector()
  static selectedWarehouse(state: WarehouseStateModel) {
    return state.selectedWarehouse;
  }
  @Selector()
  static selectedWarehouseDetail(state: WarehouseStateModel) {
    return state.selectedWarehouseDetail;
  }
  @Selector()
  static availableOwners(state: WarehouseStateModel) {
    return state.availableOwners;
  }
  @Selector()
  static selectedOwner(state: WarehouseStateModel) {
    return state.selectedOwner;
  }

@Action(AddWarehouse)
addMaterial({ dispatch }: StateContext<WarehouseStateModel>, { payload }: AddWarehouse) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehousePost(payload,Guid.newGuid()).pipe(
    tap(
      result => console.log("add warehouse"),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay, new Navigate({
          commands: ['/warehouse/list']
        })]);
      }
    )
  );
}
@Action(EditWarehouse)
editWarehouse({ dispatch, patchState }: StateContext<WarehouseStateModel>, { payload }: EditWarehouse) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehousePut(payload,Guid.newGuid()).pipe(
    tap(
      result => console.log("edit warehouse"),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        patchState({
        selectedWarehouse: null
      });
        dispatch([ResolveLoadingOverlay, new Navigate({
          commands: ['/warehouse/list']
        })]);
      }
    )
  );
}
@Action(DeleteWarehouse)
deleteWarehouse({ dispatch }: StateContext<WarehouseStateModel>, { payload }: DeleteWarehouse) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehouseDelete(payload.id).pipe(
    tap(
      result => {
        if (!result) new Alert("Something was wrong. Contact your admin.");
      },
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay, new FetchWarehouseByUser(payload.ownerId)]);
      }
    )
  );
}
@Action(FetchWarehouses)
fetchWarehouses({ dispatch,patchState }: StateContext<WarehouseStateModel>) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehouseAllGet().pipe(
    tap(
      result => patchState({
        warehouses: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay]);
      }
    )
  );
}

@Action(FetchWarehousesExcept)
fetchWarehousesExcept({ dispatch,patchState }: StateContext<WarehouseStateModel>,{payload}:FetchWarehousesExcept) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehouseExceptIdGet(payload).pipe(
    tap(
      result => patchState({
        warehouses: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay]);
      }
    )
  );
}

@Action(FetchWarehouseByUser)
fetchWarehouseByUser({ dispatch,patchState }: StateContext<WarehouseStateModel>, { payload }: FetchWarehouseByUser) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehouseUserIdGet(payload).pipe(
    tap(
      result => patchState({
        selectedWarehouse: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay]);
      }
    )
  );
}
@Action(SearchWarehouseByArea)
searchWarehouseByArea({ dispatch,patchState }: StateContext<WarehouseStateModel>, { payload }: SearchWarehouseByArea) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehouseSearchPost(payload).pipe(
    tap(
      result => patchState({
        selectedWarehouseDetail: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay, new Navigate({
          commands: ['/warehouse/book']
        })]);
      }
    )
  );
}
@Action(FetchWarehouseById)
fetchWarehouseById({ dispatch,patchState }: StateContext<WarehouseStateModel>, { payload }: FetchWarehouseById) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehouseDetailIdGet(payload).pipe(
    tap(
      result => patchState({
        selectedWarehouse: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay]);
      }
    )
  );
}
@Action(FetchOwnerById)
fetchOwnerById({ dispatch,patchState }: StateContext<WarehouseStateModel>, { payload }: FetchOwnerById) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehouseOwnerIdGet(payload).pipe(
    tap(
      result => patchState({
        selectedOwner: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay]);
      }
    )
  );
}

@Action(FetchAvailableOwners)
fetchAvailableOwners({ dispatch,patchState }: StateContext<WarehouseStateModel>) {

  dispatch(RegisterLoadingOverlay);

  return this.warehouseService.apiV1WarehouseAvailableOwnerGet().pipe(
    tap(
      result => patchState({
        availableOwners: result
      }),
      (err: HttpErrorResponse) =>
        dispatch([
          new Alert(err.error),
          ResolveLoadingOverlay
        ]),
      () => {
        dispatch([ResolveLoadingOverlay]);
      }
    )
  );
}
}

