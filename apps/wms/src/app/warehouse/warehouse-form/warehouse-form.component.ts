import { Appearance,Location } from '@angular-material-extensions/google-maps-autocomplete';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Alert, RegisterLoadingOverlay, ResolveLoadingOverlay } from '@fluctware/core';
import { Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Owner, WarehouseViewModel } from '../../services/warehouse-api';
import PlaceResult = google.maps.places.PlaceResult;

@Component({
  selector: 'fluctware-warehouse-form',
  templateUrl: './warehouse-form.component.html',
  styleUrls: ['./warehouse-form.component.scss']
})
export class WarehouseFormComponent implements OnInit, OnDestroy {
  
  @Input()
  editMode:boolean;

  @Input()
  userId:string;

  @Input()
  title:string;

  @Input()
  warehouse$:Observable<WarehouseViewModel>;
  
  @Input()
  owner$:Observable<Owner[]>;
  
  @Output()
  onSubmit=new EventEmitter<WarehouseViewModel>();

  whForm:FormGroup;

  public appearance = Appearance;
  public zoom: number;
  public latitude: number;
  public longitude: number;
  public selectedAddress: PlaceResult;
  public startTime:string;
  public endTime:string;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private fb: FormBuilder,private store:Store) { 
    this.unsubscribe$ = new ReplaySubject(1);
    this.startTime="10:00 AM";
    this.endTime="10:00 PM";
    this.whForm=this.fb.group({
      id: [""],
      name: ["",[Validators.required]],
      space: [1],
      opens: [this.startTime,[Validators.required]],
      closed: [this.endTime,[Validators.required]],
      areaLevel1: [""],
      areaLevel2: [""],
      areaLevel3: [""],
      areaLevel4: [""],
      ownerId: ["",[Validators.required]],
      latitude: [-6.175392999999975],
      longitude: [106.82702099999997],
      chiller: [0,[Validators.required]],
      freezer: [0,[Validators.required]],
    });
  }

  ngOnInit() {
    this.warehouse$?.pipe(takeUntil(this.unsubscribe$))
    .subscribe(warehouse => {
      if(warehouse!==null){
        this.whForm.patchValue(
          {
            id: warehouse?.id,
            name: warehouse?.name,
            space: warehouse?.spaceLeft,
            opens: warehouse?.opens,
            closed:warehouse?.closed,
            areaLevel1:warehouse?.areaLevel1,
            areaLevel2:warehouse?.areaLevel2,
            areaLevel3:warehouse?.areaLevel3,
            areaLevel4:warehouse?.areaLevel4,
            ownerId:warehouse?.ownerId,
            latitude:warehouse?.latitude??-6.175392999999975,
            longitude:warehouse?.longitude??106.82702099999997,
            chiller: warehouse?.chiller,
            freezer: warehouse?.freezer
          });
          this.startTime=warehouse?.opens;
          this.endTime=warehouse?.closed;
          if(warehouse?.latitude && warehouse?.longitude){
            this.latitude = warehouse?.latitude;
            this.longitude = warehouse?.longitude;
          }
          else{
            this.latitude = -6.175392999999975;
            this.longitude = 106.82702099999997;
          }
          this.zoom = 15;
        }
      });
      
      
    
    
    this.setCurrentPosition();
  }
  
  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  
  private setCurrentPosition() {
    this.store.dispatch(RegisterLoadingOverlay);
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
    this.store.dispatch(ResolveLoadingOverlay);
  }

  onAutocompleteSelected(result: PlaceResult) {
    this.whForm.patchValue(
      {
        areaLevel1:result.address_components.find(x=>this.findAreaLevel("administrative_area_level_1",x))?.long_name,
        areaLevel2:result.address_components.find(x=>this.findAreaLevel("administrative_area_level_2",x))?.long_name,
        areaLevel3:result.address_components.find(x=>this.findAreaLevel("administrative_area_level_3",x))?.long_name,
        areaLevel4:result.address_components.find(x=>this.findAreaLevel("administrative_area_level_4",x))?.long_name,
      });
      
    console.log(this.whForm.value);
  }
  findAreaLevel(key:string,geocode:google.maps.GeocoderAddressComponent){
    return geocode.types.find(x=>x===key);
  }
  onLocationSelected(location: Location) {
    this.whForm.patchValue(
      {
        latitude:location.latitude,
        longitude:location.longitude,
      });
    this.latitude = location.latitude;
    this.longitude = location.longitude;
    this.zoom = 15;
    console.log('onLocationSelected: ', location);
  }
  submit(){
    if (!this.whForm.valid) {
      // if(this.whForm.value.areaLevel1===undefined){
      //   this.store.dispatch(new Alert("Alamat belum terpilih"));
      // }else{
        this.store.dispatch(new Alert("Form belum lengkap"));
      // }
      return;
    }
    var wh:WarehouseViewModel={
      id:this.whForm.value.id,
      name: this.whForm.value.name,
      spaceLeft: this.whForm.value.space,
      opens: this.whForm.value.opens,
      closed:this.whForm.value.closed,
      areaLevel1:this.whForm.value.areaLevel1,
      areaLevel2:this.whForm.value.areaLevel2,
      areaLevel3:this.whForm.value.areaLevel3,
      areaLevel4:this.whForm.value.areaLevel4,
      ownerId:this.whForm.value.ownerId,
      latitude:this.whForm.value.latitude,
      longitude:this.whForm.value.longitude,
      chiller:this.whForm.value.chiller,
      freezer:this.whForm.value.freezer
    };
    this.onSubmit.emit(wh);
  }
  startTimeChanged(time:string){
    this.whForm.patchValue(
      {
        opens:time,
      });
  }
  endTimeChanged(time:string){
    this.whForm.patchValue(
      {
        closed:time,
      });
  }
}
