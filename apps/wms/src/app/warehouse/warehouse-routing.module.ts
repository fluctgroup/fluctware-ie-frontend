import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WarehouseAddComponent } from './warehouse-add/warehouse-add.component';
import { WarehouseBookComponent } from './warehouse-book/warehouse-book.component';
import { WarehouseEditComponent } from './warehouse-edit/warehouse-edit.component';
import { WarehouseListComponent } from './warehouse-list/warehouse-list.component';
import { WarehouseSearchComponent } from './warehouse-search/warehouse-search.component';

const routes: Routes = [
  // { path: '', component: WarehouseSearchComponent },
  { path: '', component: WarehouseListComponent },
  { path: 'book', component: WarehouseBookComponent },
  { path: 'list', component: WarehouseListComponent },
  { path: 'add', component: WarehouseAddComponent },
  { path: 'edit/:id', component: WarehouseEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WarehouseRoutingModule { }
