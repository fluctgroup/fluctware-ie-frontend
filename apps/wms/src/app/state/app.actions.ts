import { IAppSpecificConfiguration } from '../models/configuration.model';


/** Load Configuration Command */
export class LoadAppSpecificConfiguration {
  static readonly type = '[APP] LOAD CONFIGURATION';
  constructor(public payload: IAppSpecificConfiguration) { }
}
