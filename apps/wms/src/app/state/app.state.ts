import { State, Action, StateContext, Selector } from '@ngxs/store';

import { LoadAppSpecificConfiguration } from './app.actions';
import { IAppSpecificConfiguration } from '../models/configuration.model';

export interface AppStateModel {
    configuration: IAppSpecificConfiguration;
}

const defaults: AppStateModel = {
    configuration: null
};

@State<AppStateModel>({
    name: 'app',
    defaults: defaults
})
export class AppState {

    constructor() {
    }
    //#region Selectors

    @Selector()
    static configuration(state: AppStateModel) {
        return state.configuration;
    }
    //#endregion

    /** Load Configuration Command */
    @Action(LoadAppSpecificConfiguration)
    loadConfiguration(
        { patchState }: StateContext<AppStateModel>,
        { payload }: LoadAppSpecificConfiguration
    ) {
        patchState({
            configuration: payload
        });
    }
}
