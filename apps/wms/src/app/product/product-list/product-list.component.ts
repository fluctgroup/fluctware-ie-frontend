import { Component, OnDestroy, OnInit } from '@angular/core';
import { CoreState, Navigate } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProductViewModel } from '../../services/warehouse-api';
import { FetchProducts } from '../state/product.actions';
import { ProductState } from '../state/product.state';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit,OnDestroy {
  
  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(ProductState.products)
  products$: Observable<ProductViewModel[]>;

  unsubscribe$: ReplaySubject<boolean>;
  
  constructor(private store: Store) { 
    this.unsubscribe$ = new ReplaySubject(1);
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(false);
  }

  ngOnInit() {
    this.userData$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        this.store.dispatch(FetchProducts);
      });
  }
  editProduct(element:ProductViewModel){
    this.store.dispatch(new Navigate({ commands: [ 'product/edit',element.id] }));
  }
}

