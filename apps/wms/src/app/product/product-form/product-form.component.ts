import { ChangeDetectorRef, Component, EventEmitter, Input, NgZone, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Alert } from '@fluctware/core';
import { Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ScanBarcodeComponent } from '../../dialog/scan-barcode/scan-barcode.component';
import { AddProductCommand, PrincipleViewModel, ProductViewModel } from '../../services/warehouse-api';

@Component({
  selector: 'fluctware-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit, OnDestroy {
  code:string;

  @Input()
  userId: string;

  @Input()
  title: string;

  @Input()
  principle$: Observable<PrincipleViewModel>;

  @Input()
  product$: Observable<ProductViewModel>;

  @Output()
  onSubmit = new EventEmitter<any>();

  productForm: FormGroup;
  imageSrc: string;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private fb: FormBuilder, 
    private store: Store,
    public dialog: MatDialog) {
    this.unsubscribe$ = new ReplaySubject(1);
    this.productForm = this.fb.group({
      id: [""],
      name: ["", [Validators.required]],
      skuPrinciple: ["", [Validators.required]],
      principleId: ["", [Validators.required]],
      hasExpireDate: ["false", [Validators.required]],
      hasRange: ["false", [Validators.required]],
      rangeHigh: ["0"],
      rangeLow: ["0"],
      image: ["", [Validators.required]],
      authorId: [this.userId],
      lowQty: [10, [Validators.required]],
      expireDateLimit: [30, [Validators.required]],
    });
  }

  ngOnInit() {
    this.product$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(product => {
        if(product!==null){
          this.imageSrc=product.photoUrl;
          this.productForm.patchValue(
            {
              id: product?.id,
              name: product?.name,
              skuPrinciple: product?.skuPrinciple,
              principleId: product?.principleId,
              hasExpireDate: product?.hasExpireDate?"true":"false",
              hasRange: product?.hasRange?"true":"false",
              rangeHigh: product?.rangeHigh,
              rangeLow: product?.rangeLow,
              image: product?.photoUrl,
              lowQty: product?.lowQty,
              expireDateLimit: product?.expireDateLimit
            });
        }
        
      });
  }
  openDialog():void {
    const dialogRef = this.dialog.open(ScanBarcodeComponent,{
      width: '250px',
      data: {code: this.code}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.code = result;
      this.productForm.patchValue({
        skuPrinciple:this.code
      });
      this.submit();
    });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  submit() {
    if (!this.productForm.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      return;
    }
    
    let img64=this.productForm.value.image;

    // if(img64){
    //   img64=img64.split(',')[1];
    // }
    this.productForm.value.image=img64;
    console.log(this.productForm);
    this.onSubmit.emit(this.productForm);
  }
  onPrincipleChange(principle: any) {
    console.log(principle);
    this.productForm.patchValue({
      principleId: principle.value
    });
  }
  onHasExpiryChange(res: any) {
    this.productForm.patchValue({
      hasExpireDate: res.value
    });
  }
  onHasRangeChange(res: any) {
      this.productForm.patchValue({
        hasRange: res.value
      });
      console.log(this.productForm.value.hasRange);
  }
  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.productForm.patchValue({ image: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.productForm.patchValue({ image: Reader.result.toString() });
      this.imageSrc = Reader.result.toString();
    };
    Reader.readAsDataURL(file);
  }
}
