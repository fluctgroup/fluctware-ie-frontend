import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FetchPrinciple } from '../../principle/state/principle.actions';
import { PrincipleState } from '../../principle/state/principle.state';
import { EditProductCommand, PrincipleViewModel, ProductViewModel } from '../../services/warehouse-api';
import { EditProduct, FetchProductById } from '../state/product.actions';
import { ProductState } from '../state/product.state';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit, OnDestroy {

  @Select(ProductState.selectedProduct)
  product$: Observable<ProductViewModel>;

  @Select(PrincipleState.principles)
  principles$: Observable<PrincipleViewModel[]>;
  
  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  itemId: number;
  
  unsubscribe$: ReplaySubject<boolean>;

  constructor(private route: ActivatedRoute, private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  ngOnInit() {
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
      if (y) {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.itemId = Number(params.get('id'));
          this.store.dispatch([FetchPrinciple,new FetchProductById(this.itemId)]);
        });
      }
    })
  }
  submit(product:any){
    var wh: EditProductCommand = {
      productId:product.value.id,
      name: product.value.name,
      skuPrinciple: product.value.skuPrinciple,
      principleId: product.value.principleId,
      hasExpireDate: product.value.hasExpireDate==="false"?false:true,
      hasRange: product.value.hasRange==="false"?false:true,
      rangeHigh: product.value.rangeHigh,
      rangeLow: product.value.rangeLow,
      authorId:product.value.authorId===null?"anonymous":product.value.authorId,
      lowQty: product.value.lowQty,
      photoUrl:product.value.image,
      expireDateLimit:product.value.expireDateLimit
    };
    this.store.dispatch(new EditProduct(wh));
  }
}
