import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fluctware/shared';
import { ProductFormComponent } from './product-form/product-form.component';
import { ApiModule } from '../services/warehouse-api';
import { ProductRoutingModule } from './product-routing.module';
import { NgxsModule } from '@ngxs/store';
import { ProductState } from './state/product.state';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductAddComponent } from './product-add/product-add.component';
import { PrincipleState } from '../principle/state/principle.state';
import { ProductListComponent } from './product-list/product-list.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ApiModule,
    NgxsModule.forFeature([ProductState,PrincipleState]),
    ProductRoutingModule,
  ],
  declarations: [
    ProductFormComponent,
    ProductEditComponent,
    ProductAddComponent,
    ProductListComponent
  ]
})
export class ProductModule { }
