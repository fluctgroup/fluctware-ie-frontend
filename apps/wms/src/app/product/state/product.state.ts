import { State, Selector, StateContext, Action, Select } from '@ngxs/store';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CoreState, IConfiguration, RegisterLoadingOverlay, Alert, ResolveLoadingOverlay, Navigate, SessionStorageService, Guid } from '@fluctware/core';
import { Observable, ReplaySubject } from 'rxjs';
import { AddProduct, ClearProductStateData, DeleteProduct, EditProduct, FetchProductById, FetchProductBySKU, FetchProducts } from './product.actions';
import { Injectable } from '@angular/core';
import { tap, takeUntil } from 'rxjs/operators';
import { Configuration, ProductService, ProductViewModel } from '../../services/warehouse-api';

export interface ProductStateModel {
  products: ProductViewModel[];
  selectedProduct: ProductViewModel;
}
const defaults: ProductStateModel = {
  products: [],
  selectedProduct: null,
};
@State<ProductStateModel>({
  name: 'Product',
  defaults: defaults
})
@Injectable()
export class ProductState {

  @Select(CoreState.configuration)
  configurations$: Observable<IConfiguration>;

  configuration: Configuration;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(
    private httpClient: HttpClient,
    private productService: ProductService,
    private storageService: SessionStorageService
  ) {
    this.unsubscribe$ = new ReplaySubject(1);

    this.configurations$.pipe(
      takeUntil(this.unsubscribe$),
      tap(x => {
        console.log(x);
        if (x !== undefined && x !== null) {
          this.configuration = new Configuration({
            accessToken: this.storageService.retrieve(
              'authorizationData_wmsspa'
            ),
            basePath: x.warehouseUrl
          });

          this.productService = new ProductService(
            this.httpClient,
            x.warehouseUrl,
            this.configuration
          );
        }
      })
    ).subscribe();
  }

  @Selector()
  static products(state: ProductStateModel) {
    return state.products;
  }
  @Selector()
  static selectedProduct(state: ProductStateModel) {
    return state.selectedProduct;
  }

  @Action(AddProduct)
  addMaterial({ dispatch }: StateContext<ProductStateModel>, { payload }: AddProduct) {

    dispatch(RegisterLoadingOverlay);

    return this.productService.apiProductNewPost(payload, Guid.newGuid()).pipe(
      tap(
        result => console.log("add product"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['product/list']
          })]);
        }
      )
    );
  }
  @Action(EditProduct)
  editProduct({ dispatch, patchState }: StateContext<ProductStateModel>, { payload }: EditProduct) {

    dispatch(RegisterLoadingOverlay);

    return this.productService.apiProductEditPut(payload, Guid.newGuid()).pipe(
      tap(
        result => console.log("edit product"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          patchState({
            selectedProduct: null
          });
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['product/list']
          })]);
        }
      )
    );
  }

  @Action(FetchProductById)
  fetchProductById({ dispatch, patchState }: StateContext<ProductStateModel>, { payload }: FetchProductById) {

    dispatch(RegisterLoadingOverlay);

    return this.productService.apiProductDetailIdGet(payload).pipe(
      tap(
        result => patchState({
          selectedProduct: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }
  @Action(FetchProductBySKU)
  fetchProductBySKU({ dispatch, patchState }: StateContext<ProductStateModel>, { payload }: FetchProductBySKU) {

    dispatch(RegisterLoadingOverlay);

    return this.productService.apiProductSkuGet(payload).pipe(
      tap(
        result => patchState({
          selectedProduct: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }
  @Action(FetchProducts)
  fetchProducts({ dispatch, patchState }: StateContext<ProductStateModel>, { payload }: FetchProductById) {

    dispatch(RegisterLoadingOverlay);

    return this.productService.apiProductGet().pipe(
      tap(
        result => patchState({
          products: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }

  @Action(ClearProductStateData)
  clearProductStateData({ patchState }: StateContext<ProductStateModel>) {
    patchState({
      products: [],
      selectedProduct: null,
    })
  }
}
