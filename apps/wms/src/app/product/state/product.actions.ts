import { ProductViewModel } from "../../services/warehouse-api";

export class AddProduct {
  static readonly type = '[WMS] ADD PRODUCT';
  constructor(public payload: ProductViewModel) { }
}
export class EditProduct {
  static readonly type = '[WMS] EDIT PRODUCT';
  constructor(public payload: ProductViewModel) { }
}

export class DeleteProduct {
  static readonly type = '[WMS] DELETE PRODUCT';
  constructor(public payload: ProductViewModel) { }
}

export class FetchProductById {
  static readonly type = '[WMS] FETCH PRODUCT BY ID';
  constructor(public payload: number) { }
}
export class FetchProductBySKU {
  static readonly type = '[WMS] FETCH PRODUCT BY SKU';
  constructor(public payload: string) { }
}
export class FetchProducts {
  static readonly type = '[WMS] FETCH PRODUCTS';
  constructor() { }
}
export class ClearProductStateData {
  static readonly type = '[WMS] CLEAR PRODUCT STATE DATA';
  constructor() { }
}