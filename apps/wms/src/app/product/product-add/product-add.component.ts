import { Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CoreState } from "@fluctware/core";
import { Select, Store } from "@ngxs/store";
import { Observable } from "rxjs";
import { ScanBarcodeComponent } from "../../dialog/scan-barcode/scan-barcode.component";
import { FetchPrinciple } from "../../principle/state/principle.actions";
import { PrincipleState } from "../../principle/state/principle.state";
import {
  AddProductCommand,
  PrincipleViewModel,
  ProductViewModel,
} from "../../services/warehouse-api";

import { AddProduct, EditProduct } from "../state/product.actions";

@Component({
  selector: "app-product-add",
  templateUrl: "./product-add.component.html",
  styleUrls: ["./product-add.component.scss"],
})
export class ProductAddComponent implements OnInit, OnDestroy {
  @Select(PrincipleState.principles)
  principles$: Observable<PrincipleViewModel[]>;

  @Select(CoreState.userData)
  userData$: Observable<any>;

  constructor(private store: Store) {}
  ngOnDestroy(): void {}
  ngOnInit() {
    this.store.dispatch(FetchPrinciple);
  }
  submit(product: any) {
    var wh: AddProductCommand = {
      name: product.value.name,
      skuPrinciple: product.value.skuPrinciple,
      principleId: product.value.principleId,
      hasExpireDate: product.value.hasExpireDate === "false" ? false : true,
      hasRange: product.value.hasRange === "false" ? false : true,
      rangeHigh: product.value.rangeHigh,
      rangeLow: product.value.rangeLow,
      authorId:
        product.value.authorId === null ? "anonymous" : product.value.authorId,
      lowQty: product.value.lowQty,
      photoUrl: product.value.image,
      expireDateLimit:product.value.expireDateLimit
    };
    this.store.dispatch(new AddProduct(wh));
  }
}
