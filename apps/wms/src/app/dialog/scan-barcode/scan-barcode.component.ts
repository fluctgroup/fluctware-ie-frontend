import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { fromEvent } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import { ScanData } from "../../models/scan.model";

@Component({
  selector: "fluctware-scan-barcode",
  templateUrl: "./scan-barcode.component.html",
  styleUrls: ["./scan-barcode.component.scss"],
})
export class ScanBarcodeComponent implements OnInit {
  @ViewChild("input") input: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<ScanBarcodeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ScanData
  ) {}
  ngAfterViewInit() {
    fromEvent(this.input.nativeElement, "keyup")
      .pipe(
        filter(Boolean),
        debounceTime(150),
        distinctUntilChanged(),
        tap((text) => {
          this.dialogRef.close(this.input.nativeElement.value);
        })
      )
      .subscribe();
  }
  ngOnInit() {}
  onNoClick(): void {
    this.dialogRef.close();
  }
}
