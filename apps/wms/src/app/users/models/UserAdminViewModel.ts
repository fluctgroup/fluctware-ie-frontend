export interface UserAdminViewModel{
    id:string;
    email:string;
    role:string;
    paxelHome:string;
    address:string;
}