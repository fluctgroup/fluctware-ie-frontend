import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { NgxsModule } from '@ngxs/store';
import { SharedModule } from '@fluctware/shared';
import { UserState } from './state/user.state';
import { UserListComponent } from './user-list/user-list.component';
import { ApiModule } from '../services/identity-api';
import { UserFormComponent } from './user-form/user-form.component';
import { UserAddComponent } from './user-add/user-add.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ApiModule,
    NgxsModule.forFeature([UserState]),
    UsersRoutingModule
  ],
  declarations: [
    UserListComponent,
    UserFormComponent,
  UserAddComponent]
})
export class UsersModule { }
