import { RegisterViewModel } from "../../services/identity-api";

export class GetUsers {
  static readonly type = '[IDENTITY] GET USERS';
  constructor() { }
}
export class GetUserById {
  static readonly type = '[IDENTITY] GET USER BY ID';
  constructor(public payload:string) { }
}

export class GetRoles {
  static readonly type = '[IDENTITY] GET ROLES';
  constructor() { }
}

export class AddUser {
  static readonly type = '[IDENTITY] ADD USER';
  constructor(public payload:RegisterViewModel) { }
}
export class EditUser {
  static readonly type = '[IDENTITY] EDIT USER';
  constructor(public payload:{id:string;body:RegisterViewModel}) { }
}