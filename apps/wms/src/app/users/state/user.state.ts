import { State, Selector, StateContext, Action, Select } from '@ngxs/store';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CoreState, IConfiguration, RegisterLoadingOverlay, Alert, ResolveLoadingOverlay, Navigate, SessionStorageService } from '@fluctware/core';
import { Observable, ReplaySubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { tap, takeUntil } from 'rxjs/operators';
import { Configuration } from '../../services/warehouse-api';
import { AccountService, IdentityRole, UserViewModel } from '../../services/identity-api';
import { AddUser, EditUser, GetRoles, GetUserById, GetUsers } from './user.actions';

export interface UserStateModel {
  selectedUser:UserViewModel;
  users:UserViewModel[];
  roles:IdentityRole[];
}
const defaults: UserStateModel = {
  selectedUser:null,
  users:[],
  roles:[]
};
@State<UserStateModel>({
  name: 'User',
  defaults: defaults
})
@Injectable()
export class UserState {

  @Select(CoreState.configuration) 
  configurations$: Observable<IConfiguration>;

  configuration: Configuration;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(
    private httpClient: HttpClient,
    private accountService: AccountService,
    private storageService: SessionStorageService
  ) { 
    this.unsubscribe$ = new ReplaySubject(1);

    this.configurations$.pipe(
      takeUntil(this.unsubscribe$),
      tap(x => {
        console.log(x);
        if (x !== undefined && x !== null) {
          this.configuration = new Configuration({
            accessToken: this.storageService.retrieve(
              'authorizationData_wmsspa'
            ),
            basePath: x.warehouseUrl
          });
          this.accountService = new AccountService(
            this.httpClient,
            x.identityUrl,
            this.configuration
          );
        }
      })
    ).subscribe();
  }

  @Selector()
  static selectedUser(state: UserStateModel) {
    return state.selectedUser;
  }
  @Selector()
  static users(state: UserStateModel) {
    return state.users;
  }
  @Selector()
  static roles(state: UserStateModel) {
    return state.roles;
  }

  @Action(GetUserById)
  getUserById({ dispatch,patchState }: StateContext<UserStateModel>,
    { payload }: GetUserById) {
    dispatch(RegisterLoadingOverlay);
  
    return this.accountService.userIdGet(payload).pipe(
      tap(
        result => patchState({
          selectedUser:result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }

  @Action(EditUser)
  editUser({ dispatch,patchState }: StateContext<UserStateModel>,
    { payload }: EditUser) {
    dispatch(RegisterLoadingOverlay);
  
    return this.accountService.userIdPut(payload.id,payload.body).pipe(
      tap(
        result => {},
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/users/list']
          })]);
        }
      )
    );
  }
  @Action(GetUsers)
  getUsers({ dispatch,patchState }: StateContext<UserStateModel>) {
    dispatch(RegisterLoadingOverlay);
  
    return this.accountService.usersGet().pipe(
      tap(
        result => patchState({
          users:result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/users/list']
          })]);
        }
      )
    );
  }
  
  @Action(GetRoles)
  getRoles({ dispatch,patchState }: StateContext<UserStateModel>) {
    dispatch(RegisterLoadingOverlay);
  
    return this.accountService.rolesGet().pipe(
      tap(
        result => patchState({
          roles:result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }
  
  @Action(AddUser)
  addUser({ dispatch,patchState }: StateContext<UserStateModel>,
    { payload }: AddUser) {
    dispatch(RegisterLoadingOverlay);
  
    return this.accountService.registerPost(payload).pipe(
      tap(
        result => patchState({
          roles:result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/users/list']
          })]);
        }
      )
    );
  }
}

