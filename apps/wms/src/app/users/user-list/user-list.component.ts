import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { CoreState, Navigate } from "@fluctware/core";
import { Select, Store } from "@ngxs/store";
import { Observable, ReplaySubject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { UserViewModel } from "../../services/identity-api";
import {
  InventoryViewModel,
  WarehouseElementViewModel,
  WarehouseViewModel,
} from "../../services/warehouse-api";
import {
  FetchWarehouseByUser,
  FetchWarehouses,
} from "../../warehouse/state/warehouse.actions";
import { WarehouseState } from "../../warehouse/state/warehouse.state";
import { UserAdminViewModel } from "../models/UserAdminViewModel";
import { GetUsers } from "../state/user.actions";
import { UserState } from "../state/user.state";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"],
})
export class UserListComponent implements OnInit,OnDestroy {
  displayedColumns: string[] = [
    "email",
    "role",
    "paxelHome",
    "address",
    // "actions",
  ];
  dataSource: MatTableDataSource<UserAdminViewModel>;

  @Select(CoreState.userData)
  userData$: Observable<any>;

  warehouses: WarehouseViewModel[];

  @Select(WarehouseState.warehouses)
  warehouses$: Observable<WarehouseElementViewModel[]>;

  @Select(UserState.users)
  users$: Observable<UserViewModel[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  unsubscribe$: ReplaySubject<boolean>;

  constructor(private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(false);
  }

  ngOnInit() {
    this.userData$.pipe(takeUntil(this.unsubscribe$)).subscribe((vals) => {
      if (vals !== null) {
        this.store.dispatch([FetchWarehouses]);
      }
    });
    this.warehouses$.pipe(takeUntil(this.unsubscribe$)).subscribe((vals) => {
      if (vals !== null) {
        this.warehouses = vals;
        this.store.dispatch([GetUsers]);
      }
    });
    this.users$.pipe(takeUntil(this.unsubscribe$)).subscribe((vals) => {
      var outputs: UserAdminViewModel[] = [];
      vals.forEach((x) => {
        var w = this.warehouses.find((z) => z.ownerId == x.id);

        var o = <UserAdminViewModel>{
          id: x.id,
          email: x.email,
          role: x.role,
          paxelHome: w?.name,
          address:
            w?.areaLevel1 ??
            "" + ", " + w?.areaLevel2 ??
            "" + ", " + w?.areaLevel3 ??
            "" + ", " + w?.areaLevel4 ??
            "",
        };
        outputs.push(o);
      });
      this.dataSource = new MatTableDataSource(outputs);
    });
  }
  editUser(element: UserAdminViewModel) {
    this.store.dispatch(new Navigate({ commands: ["user/edit", element.id] }));
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
