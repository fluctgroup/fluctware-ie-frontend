import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Alert, CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IdentityRole, RegisterViewModel, UserViewModel } from '../../services/identity-api';
import { UserAdminViewModel } from '../models/UserAdminViewModel';
import { GetRoles } from '../state/user.actions';
import { UserState } from '../state/user.state';

@Component({
  selector: 'fluctware-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit , OnDestroy {

  @Input()
  userId: string;

  @Input()
  title: string;

  @Select(UserState.roles)
  roles$: Observable<IdentityRole[]>;

  @Select(UserState.selectedUser)
  user$: Observable<UserViewModel>;

  @Output()
  onSubmit = new EventEmitter<any>();

  userForm: FormGroup;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private fb: FormBuilder, 
    private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
    this.userForm = this.fb.group({
      email: ["", [Validators.required,Validators.email]],
      phone: ["", [Validators.required]],
      pass: ["", [Validators.required]],
      confirmPassword: ["", [Validators.required]],
      role: ["", [Validators.required]]
    });
  }

  ngOnInit() {
    this.user$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if(user!==null){
          this.userForm.patchValue(
            {
              email: user?.email,
              role:user?.role
            });
        }
        
      });
    this.store.dispatch([GetRoles]);
  }
  
  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  submit() {
    if (!this.userForm.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      return;
    }
    var user:RegisterViewModel={
      email:this.userForm.value.email,
      password:this.userForm.value.pass,
      confirmPassword:this.userForm.value.confirmPassword,
      roleId:this.userForm.value.role,
      phone:this.userForm.value.phone
    }
    this.onSubmit.emit(user);
  }
}
