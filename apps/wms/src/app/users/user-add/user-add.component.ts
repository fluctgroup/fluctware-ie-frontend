import { Component, OnDestroy, OnInit } from '@angular/core';
import { CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { RegisterViewModel } from '../../services/identity-api';
import { UserAdminViewModel } from '../models/UserAdminViewModel';
import { AddUser } from '../state/user.actions';
import { UserState } from '../state/user.state';

@Component({
  selector: 'fluctware-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit, OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  constructor(private store: Store) {}
  ngOnDestroy(): void {}
  ngOnInit() {
  }
  submit(reg: RegisterViewModel) {
    this.store.dispatch(new AddUser(reg));
  }
}