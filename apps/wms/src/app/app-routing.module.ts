import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotAuthorizedComponent, NotFoundComponent } from '@fluctware/core';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./transfer/transfer.module').then(m => m.TransferModule)
  },{
    path: 'transfer',
    loadChildren: () => import('./transfer/transfer.module').then(m => m.TransferModule)
  },{
    path: 'warehouse',
    loadChildren: () => import('./warehouse/warehouse.module').then(m => m.WarehouseModule)
  },{
    path: 'product',
    loadChildren: () => import('./product/product.module').then(m => m.ProductModule)
  },{
    path: 'inventory',
    loadChildren: () => import('./inventory/inventory.module').then(m => m.InventoryModule)
  },{
    path: 'principle',
    loadChildren: () => import('./principle/principle.module').then(m => m.PrincipleModule)
  },{
    path: 'users',
    loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
  },
  { path: 'not-authorized', component: NotAuthorizedComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
