import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Alert, CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FetchAllStockWarehouse } from '../../inventory/state/inventory.actions';
import { InventoryState } from '../../inventory/state/inventory.state';
import { FetchPrincipleById } from '../../principle/state/principle.actions';
import { PrincipleState } from '../../principle/state/principle.state';
import { FetchProductBySKU } from '../../product/state/product.actions';
import { ProductState } from '../../product/state/product.state';
import { TransportViewModel, ProductViewModel, PrincipleViewModel, InboundStockCommand, RequestInboundStockCommand, WarehouseViewModel, InboundRequestedStockCommand, ReturnRequestCommand, StockExpiryPairViewModel } from '../../services/warehouse-api';
import { FetchWarehouseByUser } from '../../warehouse/state/warehouse.actions';
import { WarehouseState } from '../../warehouse/state/warehouse.state';
import { FetchTransportDetailById, InboundRequestedStock, InboundStock, RequestInboundStock, ReturnRequest } from '../state/transfer.actions';
import { TransferState } from '../state/transfer.state';

@Component({
  selector: 'fluctware-return-request',
  templateUrl: './return-request.component.html',
  styleUrls: ['./return-request.component.scss']
})
export class ReturnRequestComponent implements OnInit, OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;
  userId: string;
  title: string;
  sku: string;
  productId: number;
  maxStock: number = 0;
  inventories:StockExpiryPairViewModel[];

  @Select(TransferState.selectedTransfer)
  transfer$: Observable<TransportViewModel>;

  @Select(ProductState.selectedProduct)
  product$: Observable<ProductViewModel>;

  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;

  @Select(PrincipleState.selectedPrinciple)
  principle$: Observable<PrincipleViewModel>;

  @Select(InventoryState.warehouseInventory)
  warehouseInventory$: Observable<StockExpiryPairViewModel[]>;

  returnRequest: FormGroup;
  imageSrc: string;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private route: ActivatedRoute, private fb: FormBuilder, private store: Store) {
    this.title = "Return Item";
    this.unsubscribe$ = new ReplaySubject(1);
    this.returnRequest = this.fb.group({
      requestId: [0],
      productId: ["", [Validators.required]],
      from: ["", [Validators.required]],
      to: ["", [Validators.required]],
      sku: ["", [Validators.required]],
      expiryDate: [new Date(0)],
      qty: [1, [Validators.required, Validators.min(1), this.stockValidator()]],
      quantifier: ["pcs", [Validators.required]]
    });
  }
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  stockValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if(this.maxStock===0) {
        this.sleep(500).then(()=>{
          const val = +control.value;
          console.log(this.maxStock)
          return val >= this.maxStock ? { maxStock: { value: control.value } } : null;    
        });
       } 
       else{
        const val = +control.value;
        console.log(this.maxStock)
        return val >= this.maxStock ? { maxStock: { value: control.value } } : null;
       }
    };
  }
  ngOnInit() {
    this.userData$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if (user) {
          this.userId = user.sub;
          this.store.dispatch(new FetchWarehouseByUser(user.sub));
        }
      })
    
    this.warehouse$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        if (vals !== null) {
          this.returnRequest.patchValue({
            from: vals.id
          });
        }
      })
      this.warehouseInventory$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(inventory => {
            if (inventory !== null) {
              this.inventories=inventory;
              this.maxStock=inventory[0].amount+1;
              this.returnRequest.patchValue({
                expiryDate: inventory[0].expiryDate,
              });
            }
          });
    this.product$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(product => {
        if (product !== null) {
          this.imageSrc = product.photoUrl;
          this.productId = product.id;
          
          this.store.dispatch(new FetchAllStockWarehouse({
            skuStock: product.skuPrinciple,
            warehouseId: this.returnRequest.value.from
          }));
          this.returnRequest.patchValue({
            sku: product.skuPrinciple,
            productId: product.id,
            to: "principle-" + product.principleId
          });
          this.store.dispatch(new FetchPrincipleById(product.principleId));
        }
      });
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
      if (y) {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.sku = params.get('id');
          if (this.sku !== undefined) {
            this.store.dispatch(new FetchProductBySKU(this.sku));
          }
        });
      }
    })
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }

  submit() {
    if (!this.returnRequest.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      console.log(this.returnRequest)
      return;
    }
    
    var cmd: ReturnRequestCommand = {
      warehouseId: this.returnRequest.value.from,
      productId: this.productId,
      authorId: this.userId,
      principle:this.returnRequest.value.to,
      notes:"",
      items: [{
        sku: this.returnRequest.value.sku,
        skuInternal: this.returnRequest.value.skuInternal,
        expiryDate: this.returnRequest.value.expiryDate,
        unique: this.returnRequest.value.unique,
        qty: this.returnRequest.value.qty,
        quantifier: this.returnRequest.value.quantifier
      }]
    };
    this.store.dispatch(new ReturnRequest(cmd));
  }
  dateChanged(event: MatDatepickerInputEvent<Date>) {
    this.returnRequest.patchValue({
      skuInternal: this.sku + "-" + event.value
    });
  }
}
