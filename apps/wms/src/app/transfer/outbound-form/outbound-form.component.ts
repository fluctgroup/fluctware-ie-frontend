import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Alert } from 'libs/core/src/lib/dialog.actions';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FetchAllStockWarehouse } from '../../inventory/state/inventory.actions';
import { InventoryState } from '../../inventory/state/inventory.state';
import { FetchPrincipleById } from '../../principle/state/principle.actions';
import { PrincipleState } from '../../principle/state/principle.state';
import { FetchProductBySKU } from '../../product/state/product.actions';
import { ProductState } from '../../product/state/product.state';
import { InventoryViewModel, OutboundRequestedStockCommand, OutboundStockCommand, PrincipleViewModel, ProductViewModel, RequestOutboundStockCommand, StockExpiryPairViewModel, TransportViewModel, WarehouseStockPairViewModel, WarehouseViewModel } from '../../services/warehouse-api';
import { FetchWarehouseByUser } from '../../warehouse/state/warehouse.actions';
import { WarehouseState } from '../../warehouse/state/warehouse.state';
import { FetchTransportDetailById, OutboundRequestedStock, OutboundStock, RequestOutboundStock } from '../state/transfer.actions';
import { TransferState } from '../state/transfer.state';

@Component({
  selector: 'fluctware-outbound-form',
  templateUrl: './outbound-form.component.html',
  styleUrls: ['./outbound-form.component.scss']
})
export class OutboundFormComponent implements OnInit, OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  skusList: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];

  itemId: number;
  userId: string;
  title: string;
  sku: string;
  productId: number;
  maxStock: number = 0;
  principleName: string;
  inventories:StockExpiryPairViewModel[];

  @Select(TransferState.selectedTransfer)
  transfer$: Observable<TransportViewModel>;

  @Select(ProductState.selectedProduct)
  product$: Observable<ProductViewModel>;

  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;

  @Select(PrincipleState.selectedPrinciple)
  principle$: Observable<PrincipleViewModel>;

  @Select(InventoryState.warehouseInventory)
  warehouseInventory$: Observable<StockExpiryPairViewModel[]>;

  outboundForm: FormGroup;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private route: ActivatedRoute, private fb: FormBuilder, private store: Store) {
    this.title = "Outbound Form";
    this.unsubscribe$ = new ReplaySubject(1);
    this.outboundForm = this.fb.group({
      requestId: [0],
      type: ["outbound"],
      name: [""],
      productId: ["", [Validators.required]],
      to: ["Client", [Validators.required]],
      from: [0, [Validators.required]],
      toId: [0],
      fromId: [0],
      status: [""],
      sku: ["", [Validators.required]],
      skuInternal: ["", [Validators.required]],
      expiryDate: [new Date(0)],
      unique: [false],
      qty: [1, [Validators.required, Validators.min(1), this.stockValidator()]],
      quantifier: ["pcs", [Validators.required]]
    });
  }
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  stockValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if(this.maxStock===0) {
        this.sleep(500).then(()=>{
          const val = +control.value;
          console.log(this.maxStock)
          return val >= this.maxStock ? { maxStock: { value: control.value } } : null;    
        });
       } 
       else{
        const val = +control.value;
        console.log(this.maxStock)
        return val >= this.maxStock ? { maxStock: { value: control.value } } : null;
       }
    };
  }
  ngOnInit() {
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
      if (y) {
        this.warehouse$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(vals => {
            if (vals !== null) {
              this.outboundForm.patchValue({
                from: vals.id
              });
            }
          });
        this.userData$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(user => {
            if (user) {
              this.userId = user.sub;
              this.store.dispatch(new FetchWarehouseByUser(user.sub));
            }
          });
        this.principle$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(principle => {
            if (principle !== null) {
              this.principleName = principle.name;
            }
          })
        this.warehouseInventory$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(inventory => {
            if (inventory !== null) {
              this.inventories=inventory;
              this.maxStock=inventory[0].amount+1;
              this.outboundForm.patchValue({
                expiryDate: inventory[0].expiryDate,
              });
            }
          })
        this.product$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(product => {
            if (product !== null) {
              this.productId = product.id;
              this.store.dispatch(new FetchAllStockWarehouse({
                skuStock: product.skuPrinciple,
                warehouseId: this.outboundForm.value.from
              }));
              this.outboundForm.patchValue({
                sku: product.skuPrinciple,
                skuInternal: product.skuPrinciple,
                productId: product.id,
              });
              this.store.dispatch(new FetchPrincipleById(product.principleId));
            }
          });
        this.transfer$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(outbound => {
            if (outbound !== null) {
              this.outboundForm.patchValue(
                {
                  requestId: outbound?.requestId,
                  type:outbound?.type,
                  name:"system",
                  to: outbound?.to,
                  toId: outbound?.toId,
                  from: outbound?.from,
                  fromId: outbound?.fromId,
                  status: outbound?.status,
                  sku: outbound?.items[0].sku,
                  skuInternal: outbound?.items[0].skuInternal,
                  expiryDate: outbound?.items[0].expiryDate,
                  unique: outbound?.items[0].unique,
                  qty: outbound?.items[0].qty,
                  quantifier: outbound?.items[0].quantifier
                });
                if(outbound.status==='waitingactions'){
                  this.outboundForm.controls['qty'].disable();
                  this.outboundForm.controls['quantifier'].disable();
                }
            }
          });
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.sku = params.get('sku');
          this.itemId = Number(params.get('id'));
          if (this.sku !== undefined) {
            this.store.dispatch(new FetchProductBySKU(this.sku));
          }
          if (this.itemId !== 0) {
            this.store.dispatch(new FetchTransportDetailById(this.itemId));
          }
        });

      }
    })


  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  submit() {
    if (!this.outboundForm.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      return;
    }
    if (this.outboundForm.value.to === "Scrap" ||
      this.outboundForm.value.to === "Principle") {
      var to = this.outboundForm.value.to;
      if (this.outboundForm.value.to === "Principle") {
        to = this.principleName;
      }
      var cmd: OutboundStockCommand = {
        warehouseId: this.outboundForm.value.from,
        productId: this.outboundForm.value.productId,
        authorId: this.userId,
        to: to,
        items: [{
          sku: this.outboundForm.value.sku,
          skuInternal: this.outboundForm.value.skuInternal,
          expiryDate: this.outboundForm.value.expiryDate,
          unique: this.outboundForm.value.unique,
          qty: this.outboundForm.value.qty,
          quantifier: this.outboundForm.value.quantifier
        }]
      };
      this.store.dispatch(new OutboundStock(cmd));
    }
    else if (this.outboundForm.value.to === "Client") {
      var cmd: OutboundStockCommand = {
        warehouseId: this.outboundForm.value.from,
        productId: this.outboundForm.value.productId,
        authorId: this.userId,
        to: this.outboundForm.value.to + "-" + this.outboundForm.value.name,
        items: [{
          sku: this.outboundForm.value.sku,
          skuInternal: this.outboundForm.value.skuInternal,
          expiryDate: this.outboundForm.value.expiryDate,
          unique: this.outboundForm.value.unique,
          qty: this.outboundForm.value.qty,
          quantifier: this.outboundForm.value.quantifier
        }]
      };
      this.store.dispatch(new OutboundStock(cmd));
    }
    else {
      var reqCmd: OutboundRequestedStockCommand = {
        warehouseId: this.outboundForm.value.fromId,
        requestId: this.outboundForm.value.requestId,
        authorId: this.userId,
        notes: ""
      }

      this.store.dispatch(new OutboundRequestedStock(reqCmd));
    }
  }
  onTransferTypeChanged(res: any) {
    this.outboundForm.patchValue({
      to: res.value
    });
  }
  onStockSelected(data:any){
    this.maxStock=this.inventories.find(x=>x.expiryDate==data.value).amount+1;
    this.outboundForm.patchValue({
      expiryDate: data.value,
    });
  }
  formatStockData(data:StockExpiryPairViewModel){
    if(data.expiryDate.toString()=="1970-01-01T00:00:00"){
      return "No Expiry Date";
    }
    return data.expiryDate;
  }
}
