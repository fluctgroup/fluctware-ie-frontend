import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Alert, CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { delay, takeUntil } from 'rxjs/operators';
import { FetchAllStockWarehouseByExpiry } from '../../inventory/state/inventory.actions';
import { InventoryState } from '../../inventory/state/inventory.state';
import { FetchPrincipleById } from '../../principle/state/principle.actions';
import { PrincipleState } from '../../principle/state/principle.state';
import { FetchProductBySKU } from '../../product/state/product.actions';
import { ProductState } from '../../product/state/product.state';
import { PrincipleViewModel, ProductViewModel, RequestOutboundStockCommand, StockExpiryPairViewModel, WarehouseStockPairViewModel, WarehouseViewModel } from '../../services/warehouse-api';
import { FetchWarehouseByUser } from '../../warehouse/state/warehouse.actions';
import { WarehouseState } from '../../warehouse/state/warehouse.state';
import { RequestOutboundStock } from '../state/transfer.actions';

@Component({
  selector: 'app-outbound-request-form',
  templateUrl: './outbound-request-form.component.html',
  styleUrls: ['./outbound-request-form.component.scss']
})
export class OutboundRequestFormComponent implements OnInit, OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;

  @Select(InventoryState.warehouseInventories)
  warehouseInventories$: Observable<WarehouseStockPairViewModel[]>;

  @Select(ProductState.selectedProduct)
  product$: Observable<ProductViewModel>;

  @Select(PrincipleState.selectedPrinciple)
  principle$: Observable<PrincipleViewModel>;

  itemId: number;
  userId: string;
  any: boolean;
  start: Date;
  end: Date;
  title: string;
  sku: string;
  productId: number;
  maxStock: number=0;
  principleName: string;
  requestOutboundForm: FormGroup;
  stocks: StockExpiryPairViewModel[];
  warehouseId: number;
  unsubscribe$: ReplaySubject<boolean>;
  constructor(private route: ActivatedRoute, private fb: FormBuilder, private store: Store) {
    this.title = "Request Outbound Form";
    this.unsubscribe$ = new ReplaySubject(1);
    this.requestOutboundForm = this.fb.group({
      productId: ["", [Validators.required]],
      to: ["", [Validators.required]],
      from: [0, [Validators.required]],
      sku: ["", [Validators.required]],
      skuInternal: ["", [Validators.required]],
      expiryDate: [new Date(0)],
      unique: [false],
      qty: [1, [Validators.required, Validators.min(1), this.stockValidator()]],
      quantifier: ["pcs", [Validators.required]]
    })
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(false);
  }
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  stockValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if(this.maxStock===0) {
        this.sleep(500).then(()=>{
          const val = +control.value;
          console.log(this.maxStock)
          return val >= this.maxStock ? { maxStock: { value: control.value } } : null;    
        });
       } 
       else{
        const val = +control.value;
        console.log(this.maxStock)
        return val >= this.maxStock ? { maxStock: { value: control.value } } : null;
       }
    };
  }
  ngOnInit() {
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
      if (y) {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.sku = params.get('sku');
          this.itemId = Number(params.get('id'));
          this.any = Boolean(params.get('any'));
          this.start = new Date(params.get('start'));
          this.end = new Date(params.get('end'));
          if (this.sku !== undefined) {
            this.store.dispatch(new FetchProductBySKU(this.sku));
          }
        });
        this.warehouse$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(vals => {
            if (vals !== null) {
              this.warehouseId = vals.id;
              this.requestOutboundForm.patchValue({
                to: vals.id
              });
              this.store.dispatch(new FetchAllStockWarehouseByExpiry({
                skuStock: this.sku,
                startDate: this.start,
                endDate: this.end,
                any: this.any,
                inquirer: this.warehouseId
              }));
            }
          });
        this.userData$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(user => {
            if (user) {
              this.userId = user.sub;
              this.store.dispatch(new FetchWarehouseByUser(user.sub));
            }
          });
        this.principle$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(principle => {
            if (principle !== null) {
              this.principleName = principle.name;
            }
          });

        this.product$?.pipe(takeUntil(this.unsubscribe$))
          .subscribe(product => {
            if (product !== null) {
              this.productId = product.id;
              this.requestOutboundForm.patchValue({
                sku: product.skuPrinciple,
                skuInternal: product.skuPrinciple,
                productId: product.id
              });
              this.store.dispatch(new FetchPrincipleById(product.principleId));
            }
          });
      }
    })
  }

  submit() {
    if (!this.requestOutboundForm.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      return;
    }
    var reqCmd: RequestOutboundStockCommand = {
      warehouseId: this.requestOutboundForm.value.from,
      productId: this.productId,
      to: this.requestOutboundForm.value.to.toString(),
      items: [{
        sku: this.requestOutboundForm.value.sku,
        skuInternal: this.requestOutboundForm.value.skuInternal,
        expiryDate: this.requestOutboundForm.value.expiryDate,
        unique: this.requestOutboundForm.value.unique,
        qty: this.requestOutboundForm.value.qty,
        quantifier: this.requestOutboundForm.value.quantifier
      }]
    }
    this.store.dispatch(new RequestOutboundStock(reqCmd));
  }
  onWarehouseSelected(data: any) {
    this.stocks = data.value.stocks;
    this.requestOutboundForm.patchValue({
      from: data.value.warehouseId,
    });
  }
  onStockSelected(data: any) {
    this.maxStock = data.value.amount+1;
    this.requestOutboundForm.patchValue({
      expiryDate: data.value.expiryDate,
    });
  }
  formatStockData(data: StockExpiryPairViewModel) {
    if (data.expiryDate.toString() == "1970-01-01T00:00:00") {
      return "No Expiry Date - " + data.amount + this.requestOutboundForm.value.quantifier;
    }
    return data.expiryDate + " - " + data.amount + this.requestOutboundForm.value.quantifier;
  }
}
