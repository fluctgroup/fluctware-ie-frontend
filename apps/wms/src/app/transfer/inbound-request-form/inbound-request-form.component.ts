import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CoreState, Alert } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { delay, takeUntil } from 'rxjs/operators';
import { FetchAllStockWarehouse, FetchAllStockWarehouseByExpiry } from '../../inventory/state/inventory.actions';
import { InventoryState } from '../../inventory/state/inventory.state';
import { FetchPrincipleById } from '../../principle/state/principle.actions';
import { PrincipleState } from '../../principle/state/principle.state';
import { FetchProductBySKU } from '../../product/state/product.actions';
import { ProductState } from '../../product/state/product.state';
import { TransportViewModel, ProductViewModel, PrincipleViewModel, InboundStockCommand, RequestInboundStockCommand, WarehouseViewModel, StockExpiryPairViewModel, WarehouseStockPairViewModel, WarehouseElementViewModel } from '../../services/warehouse-api';
import { FetchWarehouseByUser, FetchWarehouses, FetchWarehousesExcept } from '../../warehouse/state/warehouse.actions';
import { WarehouseState } from '../../warehouse/state/warehouse.state';
import { FetchTransportDetailById, InboundStock, RequestInboundStock } from '../state/transfer.actions';
import { TransferState } from '../state/transfer.state';

@Component({
  selector: 'app-inbound-request-form',
  templateUrl: './inbound-request-form.component.html',
  styleUrls: ['./inbound-request-form.component.scss']
})
export class InboundRequestFormComponent implements OnInit,OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  itemId: number;
  userId: string;
  any: boolean;
  start: Date;
  end: Date;
  title: string;
  sku: string;
  productId: number;
  maxStock: number=0;
  principleName: string;
  inboundForm: FormGroup;
  warehouseId: number;
  unsubscribe$: ReplaySubject<boolean>;

  @Select(TransferState.selectedTransfer)
  transfer$: Observable<TransportViewModel>;

  @Select(ProductState.selectedProduct)
  product$:Observable<ProductViewModel>;

  @Select(WarehouseState.warehouses)
  warehouses$: Observable<WarehouseElementViewModel[]>;
  
  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;
  
  @Select(InventoryState.warehouseInventory)
  warehouseInventory$: Observable<StockExpiryPairViewModel[]>;

  @Select(PrincipleState.selectedPrinciple)
  principle$:Observable<PrincipleViewModel>;

  constructor(private route: ActivatedRoute,private fb: FormBuilder, private store: Store) {
    this.title="Inbound Form";
    this.unsubscribe$ = new ReplaySubject(1);
    this.inboundForm = this.fb.group({
      productId: ["", [Validators.required]],
      to: ["", [Validators.required]],
      from: [0, [Validators.required]],
      sku: ["", [Validators.required]],
      skuInternal: ["", [Validators.required]],
      expiryDate: [new Date(0)],
      unique: [false],
      qty: [1, [Validators.required,Validators.min(1), this.stockValidator()]],
      quantifier: ["pcs", [Validators.required]]
    });
  }
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  stockValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if(this.maxStock===0) {
        this.sleep(500).then(()=>{
          const val = +control.value;
          console.log(this.maxStock)
          return val >= this.maxStock ? { maxStock: { value: control.value } } : null;    
        });
       } 
       else{
        const val = +control.value;
        console.log(this.maxStock)
        return val >= this.maxStock ? { maxStock: { value: control.value } } : null;
       }
    };
  }
  ngOnInit() {
  this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
    if (y) {
      this.route.paramMap.subscribe((params: ParamMap) => {
        this.sku = params.get('sku');
        if (this.sku !== undefined) {
          this.store.dispatch(new FetchProductBySKU(this.sku));
        }
      });
      this.userData$?.pipe(takeUntil(this.unsubscribe$))
    .subscribe(user => {
      if (user) {
        this.userId = user.sub;
        this.store.dispatch(new FetchWarehouseByUser(user.sub));
      }
    });
  this.principle$?.pipe(takeUntil(this.unsubscribe$))
    .subscribe(principle => {
      if (principle !== null) {
        this.principleName = principle.name;
      }
    });
  
  this.product$?.pipe(takeUntil(this.unsubscribe$))
    .subscribe(product => {
      if (product !== null) {
        this.productId = product.id;
        this.inboundForm.patchValue({
          sku: product.skuPrinciple,
          skuInternal: product.skuPrinciple,
          productId: product.id
        });
        this.store.dispatch(new FetchPrincipleById(product.principleId));
      }
    });
    this.warehouse$?.pipe(takeUntil(this.unsubscribe$))
    .subscribe(vals => {
      if (vals !== null) {
        this.warehouseId = vals.id;
        this.inboundForm.patchValue({
          from: vals.id
        });
        
        this.store.dispatch(new FetchWarehousesExcept(this.warehouseId));
        this.store.dispatch(new FetchAllStockWarehouse({
          skuStock: this.sku,
          warehouseId: this.warehouseId
        }));
      }
    });
    }
  })
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }

  submit() {
    if (!this.inboundForm.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      return;
    }
    
    var reqCmd:RequestInboundStockCommand={
      warehouseId:this.inboundForm.value.from,
      productId: this.inboundForm.value.productId,
      to: this.inboundForm.value.to,
      items: [{
        sku: this.inboundForm.value.sku,
        skuInternal: this.inboundForm.value.skuInternal,
        expiryDate: this.inboundForm.value.expiryDate,
        unique: this.inboundForm.value.unique,
        qty: this.inboundForm.value.qty,
        quantifier: this.inboundForm.value.quantifier
      }]
    }
    this.store.dispatch(new RequestInboundStock(reqCmd));
  }
  onWarehouseSelected(data:any){
    this.inboundForm.patchValue({
      to: data.value.id,
    });
  }
  onStockSelected(data:any){
    this.maxStock=data.value.amount+1;
    this.inboundForm.patchValue({
      expiryDate: data.value.expiryDate,
    });
  }
  formatStockData(data:StockExpiryPairViewModel){
    if(data.expiryDate.toString()=="1970-01-01T00:00:00"){
      return "No Expiry Date - "+ data.amount + this.inboundForm.value.quantifier;
    }
    return data.expiryDate+ " - "+ data.amount + this.inboundForm.value.quantifier;
  }
}
