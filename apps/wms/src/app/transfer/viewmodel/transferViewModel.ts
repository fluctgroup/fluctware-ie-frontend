import { TransportRequestItem } from "../../services/warehouse-api";

export class TransferViewModel {
    from?: number;
    to?: number;
    productId?: number;
    items?: Array<TransportRequestItem>;
    notes?: string;
}
