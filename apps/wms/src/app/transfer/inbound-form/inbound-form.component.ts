import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Alert, CoreState } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FetchPrincipleById } from '../../principle/state/principle.actions';
import { PrincipleState } from '../../principle/state/principle.state';
import { FetchProductBySKU } from '../../product/state/product.actions';
import { ProductState } from '../../product/state/product.state';
import { TransportViewModel, ProductViewModel, PrincipleViewModel, InboundStockCommand, RequestInboundStockCommand, WarehouseViewModel, InboundRequestedStockCommand } from '../../services/warehouse-api';
import { FetchWarehouseByUser } from '../../warehouse/state/warehouse.actions';
import { WarehouseState } from '../../warehouse/state/warehouse.state';
import { FetchTransportDetailById, InboundRequestedStock, InboundStock, RequestInboundStock } from '../state/transfer.actions';
import { TransferState } from '../state/transfer.state';

@Component({
  selector: 'fluctware-inbound-form',
  templateUrl: './inbound-form.component.html',
  styleUrls: ['./inbound-form.component.scss']
})
export class InboundFormComponent implements OnInit, OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;
  skusList: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  itemId: number;
  userId: string;
  title: string;
  sku: string;
  productId: number;

  @Select(TransferState.selectedTransfer)
  transfer$: Observable<TransportViewModel>;

  @Select(ProductState.selectedProduct)
  product$: Observable<ProductViewModel>;

  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;

  @Select(PrincipleState.selectedPrinciple)
  principle$: Observable<PrincipleViewModel>;

  inboundForm: FormGroup;
  imageSrc: string;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private route: ActivatedRoute, private fb: FormBuilder, private store: Store) {
    this.title = "Inbound Form";
    this.unsubscribe$ = new ReplaySubject(1);
    this.inboundForm = this.fb.group({
      requestId: [0],
      type: ["inbound"],
      productId: ["", [Validators.required]],
      to: ["", [Validators.required]],
      from: ["", [Validators.required]],
      toId: [0],
      fromId: [0],
      status: [""],
      sku: ["", [Validators.required]],
      skuInternal: ["", [Validators.required]],
      expiryDate: [new Date(0)],
      unique: [false],
      qty: [1, [Validators.required]],
      quantifier: ["pcs", [Validators.required]],
      image: [""],
    });
  }

  ngOnInit() {
    this.userData$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if (user) {
          this.userId = user.sub;
          this.store.dispatch(new FetchWarehouseByUser(user.sub));
        }
      })
    
    this.warehouse$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        if (vals !== null) {
          this.inboundForm.patchValue({
            to: vals.id
          });
        }
      });
    this.product$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(product => {
        if (product !== null) {
          if(product.hasExpireDate){
            const today = new Date();
            const date = today.getDate();
            const month = today.getMonth();
            const year = today.getFullYear();
            this.inboundForm.patchValue({
              expiryDate: new Date(year, month + 1, date)
            });
          }
          this.imageSrc = product.photoUrl;
          this.productId = product.id;
          this.inboundForm.patchValue({
            image: product?.photoUrl,
            sku: product.skuPrinciple,
            skuInternal: product.skuPrinciple,
            productId: product.id,
            from: "principle-" + product.principleId
          });
          this.store.dispatch(new FetchPrincipleById(product.principleId));
        }
      });
    this.isReady$.pipe(takeUntil(this.unsubscribe$)).subscribe(y => {
      if (y) {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.sku = params.get('sku');
          this.itemId = Number(params.get('id'));
          if (this.sku !== undefined) {
            this.store.dispatch(new FetchProductBySKU(this.sku));
          }
          if (this.itemId !== 0) {
            this.store.dispatch(new FetchTransportDetailById(this.itemId));
          }
        });
      }
    })

    this.transfer$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(inbound => {
        if (inbound !== null) {
          this.inboundForm.patchValue(
            {
              requestId: inbound?.requestId,
              type:inbound?.type,
              to: inbound?.to,
              from: inbound?.from,
              toId: inbound?.toId,
              fromId: inbound?.fromId,
              status: inbound?.status,
              sku: inbound?.items[0].sku,
              skuInternal: inbound?.items[0].skuInternal,
              expiryDate: inbound?.items[0].expiryDate,
              unique: inbound?.items[0].unique,
              qty: inbound?.items[0].qty,
              quantifier: inbound?.items[0].quantifier
            });
            if(inbound.status==='ondelivery'){
              this.inboundForm.controls['qty'].disable();
              this.inboundForm.controls['quantifier'].disable();
              this.inboundForm.controls['expiryDate'].disable();
            }
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }

  submit() {
    if (!this.inboundForm.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      console.log(this.inboundForm)
      return;
    }
    if (this.inboundForm.value.status==='ondelivery') {
      var cmdInbound: InboundRequestedStockCommand = {
        warehouseId: this.inboundForm.value.fromId,
        requestId: this.inboundForm.value.requestId,
        authorId: this.userId,
        notes:""
      };
      this.store.dispatch(new InboundRequestedStock(cmdInbound));
    }
    else{
      var cmd: InboundStockCommand = {
        warehouseId: this.inboundForm.value.to,
        productId: this.productId,
        authorId: this.userId,
        from: this.inboundForm.value.from,
        items: [{
          sku: this.inboundForm.value.sku,
          skuInternal: this.inboundForm.value.skuInternal,
          expiryDate: this.inboundForm.value.expiryDate,
          unique: this.inboundForm.value.unique,
          qty: this.inboundForm.value.qty,
          quantifier: this.inboundForm.value.quantifier
        }]
      };
      this.store.dispatch(new InboundStock(cmd));
    }
  }
  onTransferTypeChanged(res: any) {
    this.inboundForm.patchValue(
      {
        from: res.value
      });
  }
  dateChanged(event: MatDatepickerInputEvent<Date>) {
    this.inboundForm.patchValue({
      skuInternal: this.sku + "-" + event.value
    });
  }
}
