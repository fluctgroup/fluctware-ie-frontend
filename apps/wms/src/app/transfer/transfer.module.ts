import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TransferState } from "./state/transfer.state";
import { OutboundFormComponent } from "./outbound-form/outbound-form.component";
import { InboundFormComponent } from "./inbound-form/inbound-form.component";
import { OutboundRequestFormComponent } from "./outbound-request-form/outbound-request-form.component";
import { TransferRoutingModule } from "./transfer-routing.module";
import { SharedModule } from "@fluctware/shared";
import { NgxsModule } from "@ngxs/store";
import { ApiModule } from "../services/warehouse-api";
import { ScanTransferComponent } from "./scan-transfer/scan-transfer.component";
import { InboundRequestFormComponent } from "./inbound-request-form/inbound-request-form.component";
import { ProductState } from "../product/state/product.state";
import { PrincipleState } from "../principle/state/principle.state";
import { InventoryState } from "../inventory/state/inventory.state";
import { WarehouseState } from "../warehouse/state/warehouse.state";
import { TransferRequestComponent } from "./transfer-request/transfer-request.component";
import { TransferHistoryComponent } from "./transfer-history/transfer-history.component";
import { ReturnRequestComponent } from "./return-request/return-request.component";

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ApiModule,
    NgxsModule.forFeature([
      ProductState,
      InventoryState,
      WarehouseState,
      PrincipleState,
      TransferState,
    ]),
    TransferRoutingModule,
  ],
  declarations: [
    OutboundFormComponent,
    InboundFormComponent,
    OutboundRequestFormComponent,
    InboundRequestFormComponent,
    ScanTransferComponent,
    TransferRequestComponent,
    TransferHistoryComponent,
    ReturnRequestComponent,
  ],
})
export class TransferModule {}
