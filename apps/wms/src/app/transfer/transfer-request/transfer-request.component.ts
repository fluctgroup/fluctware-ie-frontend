import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CoreState, Navigate } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ApproveRequestInboundStockCommand, ApproveRequestOutboundStockCommand, CancelRequestTransportCommand, RejectRequestInboundStockCommand, RejectRequestOutboundStockCommand, TransportViewModel, WarehouseViewModel } from '../../services/warehouse-api';
import { FetchWarehouseByUser } from '../../warehouse/state/warehouse.actions';
import { WarehouseState } from '../../warehouse/state/warehouse.state';
import { ApproveRequestInboundStock, ApproveRequestOutboundStock, CancelRequestTransportStock, FetchHistoryTransportByWarehouseId, FetchOngoingTransportByWarehouseId, RejectRequestInboundStock, RejectRequestOutboundStock } from '../state/transfer.actions';
import { TransferState } from '../state/transfer.state';

@Component({
  selector: 'fluctware-transfer-request',
  templateUrl: './transfer-request.component.html',
  styleUrls: ['./transfer-request.component.scss']
})
export class TransferRequestComponent implements OnInit {
  displayedColumns: string[] = ['inboundTime','to','from','status','type', 'items','actions'];
  dataSource: MatTableDataSource<TransportViewModel>;

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;

  @Select(TransferState.ongoingTransfer)
  transfers$: Observable<TransportViewModel[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  warehouseId: number;
  warehouseName:string;
  unsubscribe$: ReplaySubject<boolean>;
  constructor(private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  ngOnInit(): void {
    this.userData$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if (user) {
          this.store.dispatch(new FetchWarehouseByUser(user.sub));
        }
      })
    this.warehouse$.pipe(takeUntil(this.unsubscribe$))
    .subscribe(w => {
      if (w) {
        this.warehouseId = w.id;
        this.warehouseName=w.name;
        this.store.dispatch(new FetchOngoingTransportByWarehouseId(w.id));
      }
    });
    this.transfers$.pipe(takeUntil(this.unsubscribe$))
    .subscribe(transfer => {
      this.dataSource = new MatTableDataSource(transfer);
    });
    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onReject(data:TransportViewModel){
    if(data.type==='outbound'){
      var rejectOutbound:RejectRequestOutboundStockCommand={
        warehouseId: this.warehouseId,
        requestId: data.requestId
      }
      this.store.dispatch(new RejectRequestOutboundStock(rejectOutbound));
    }
    else if(data.type==='inbound'){
      var rejectInbound:RejectRequestInboundStockCommand={
        warehouseId: data.fromId,
        requestId: data.requestId
      }
      this.store.dispatch(new RejectRequestInboundStock(rejectInbound));
    }
  }
  onApprove(data:TransportViewModel){
    if(data.type==='outbound'){
      var approveOutbound:ApproveRequestOutboundStockCommand={
        warehouseId: this.warehouseId,
        requestId: data.requestId
      }
      this.store.dispatch(new ApproveRequestOutboundStock(approveOutbound));
    }
    else if(data.type==='inbound'){
      var approveInbound:ApproveRequestInboundStockCommand={
        warehouseId: data.fromId,
        requestId: data.requestId
      }
      this.store.dispatch(new ApproveRequestInboundStock(approveInbound));
    }
  }
  onOutbound(data:TransportViewModel){
    // redirect to outbound page
    this.store.dispatch(new Navigate({
      commands: ['/transfer/outbound', { sku: data.items[0].sku,id: data.items[0].requestId }]
    }))
  }
  onInbound(data:TransportViewModel){
    // redirect to inbound page
    this.store.dispatch(new Navigate({
      commands: ['/transfer/inbound', { sku: data.items[0].sku,id: data.items[0].requestId }]
    }))
  }
  onCancel(data:TransportViewModel){
    var cancelOutbound:CancelRequestTransportCommand={
      warehouseId: this.warehouseId,
      requestId: data.requestId
    }
    this.store.dispatch(new CancelRequestTransportStock(cancelOutbound));
  }
  isApprovable(data:TransportViewModel){
    if(data.type==="inbound" && data.status==='waitingapproval' && !this.isIssuer(data)){
      return true;
    }
    else if(data.type==="outbound" && data.status==='waitingapproval' && !this.isIssuer(data)){
      return true;
    }
    return false;
  }
  isApproved(data:TransportViewModel,type:string){
    if(data.type==='outbound' && data.status=='waitingactions' && !this.isIssuer(data)){
      return data.type===type?true:false;
    }
    else if(data.type==='outbound' && data.status=='ondelivery' && this.isIssuer(data)){
      return type==="inbound" ?true:false;
    }
    else if(data.type==='inbound' && data.status=='waitingactions' && this.isIssuer(data)){
      return data.type===type?false:true;
    }
    else if(data.type==='inbound' && data.status=='ondelivery' && !this.isIssuer(data)){
      return data.type===type ?true:false;
    }
    return false;
  }
  
  isCancellable(data:TransportViewModel){
    if(data.type==="outbound" && data.status==='waitingapproval' && this.isIssuer(data)){
      return true;
    }
    else if(data.type==="inbound" && data.status==='waitingapproval' && this.isIssuer(data)){
      return true;
    }
    return false;
  }
  isIssuer(data:TransportViewModel){
    if(data.type==='outbound'){
      return data.to===this.warehouseName;
    }
    else if(data.type==='inbound'){
      return data.from===this.warehouseName;
    }
    return false;
  }
}
