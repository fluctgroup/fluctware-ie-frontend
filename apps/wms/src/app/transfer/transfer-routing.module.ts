import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InboundFormComponent } from './inbound-form/inbound-form.component';
import { InboundRequestFormComponent } from './inbound-request-form/inbound-request-form.component';
import { OutboundFormComponent } from './outbound-form/outbound-form.component';
import { OutboundRequestFormComponent } from './outbound-request-form/outbound-request-form.component';
import { ReturnRequestComponent } from './return-request/return-request.component';
import { ScanTransferComponent } from './scan-transfer/scan-transfer.component';
import { TransferHistoryComponent } from './transfer-history/transfer-history.component';
import { TransferRequestComponent } from './transfer-request/transfer-request.component';

const routes: Routes = [
  { path: '', component: ScanTransferComponent },
  { path: 'outbound/:id', component: OutboundFormComponent },
  { path: 'outbound', component: OutboundFormComponent },
  { path: 'inbound/:id', component: InboundFormComponent },
  { path: 'inbound', component: InboundFormComponent },
  { path: 'inbound-request', component: InboundRequestFormComponent },
  { path: 'outbound-request', component: OutboundRequestFormComponent },
  { path: 'scan', component: ScanTransferComponent },
  { path: 'history', component: TransferHistoryComponent },
  { path: 'request', component: TransferRequestComponent },
  { path: 'return/:id', component: ReturnRequestComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransferRoutingModule { }
