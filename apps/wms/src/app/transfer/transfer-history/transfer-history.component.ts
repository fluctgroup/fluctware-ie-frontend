import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { TransportViewModel, WarehouseViewModel } from '../../services/warehouse-api';
import { TransferState } from '../state/transfer.state';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FetchHistoryTransportByWarehouseId } from '../state/transfer.actions';
import { FetchWarehouseByUser } from '../../warehouse/state/warehouse.actions';
import { CoreState } from '@fluctware/core';
import { WarehouseState } from '../../warehouse/state/warehouse.state';

@Component({
  selector: 'fluctware-transfer-history',
  templateUrl: './transfer-history.component.html',
  styleUrls: ['./transfer-history.component.scss']
})
export class TransferHistoryComponent implements AfterViewInit, OnInit,OnDestroy {
  displayedColumns: string[] = ['inboundTime','to','from','status','type', 'items'];
  dataSource: MatTableDataSource<TransportViewModel>;

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;

  @Select(TransferState.historyTransfer)
  transfers$: Observable<TransportViewModel[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private store: Store) {
    this.unsubscribe$ = new ReplaySubject(1);
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
  }
  ngOnInit(): void {
    this.userData$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if (user) {
          this.store.dispatch(new FetchWarehouseByUser(user.sub));
        }
      })
    this.warehouse$.pipe(takeUntil(this.unsubscribe$))
    .subscribe(w => {
      if (w) {
        this.store.dispatch(new FetchHistoryTransportByWarehouseId(w.id));
      }
    });
    this.transfers$.pipe(takeUntil(this.unsubscribe$))
    .subscribe(transfer => {
      this.dataSource = new MatTableDataSource(transfer);
    });
    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
