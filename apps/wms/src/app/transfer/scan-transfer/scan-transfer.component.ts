import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Alert, CoreState, Navigate } from '@fluctware/core';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ScanBarcodeComponent } from '../../dialog/scan-barcode/scan-barcode.component';
import { ClearInventoryStateData, FetchAllStockWarehouse, FetchAllStockWarehouseByExpiry } from '../../inventory/state/inventory.actions';
import { InventoryState } from '../../inventory/state/inventory.state';
import { ClearProductStateData, FetchProductById, FetchProductBySKU } from '../../product/state/product.actions';
import { ProductState } from '../../product/state/product.state';
import { InventoryViewModel, ProductViewModel, StockExpiryPairViewModel, WarehouseStockPairViewModel, WarehouseViewModel } from '../../services/warehouse-api';
import { FetchWarehouseByUser } from '../../warehouse/state/warehouse.actions';
import { WarehouseState } from '../../warehouse/state/warehouse.state';
import { ClearTransportStateData } from '../state/transfer.actions';

@Component({
  selector: 'app-scan-transfer',
  templateUrl: './scan-transfer.component.html',
  styleUrls: ['./scan-transfer.component.scss']
})
export class ScanTransferComponent implements OnInit,OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  @Select(CoreState.isReady)
  isReady$: Observable<boolean>;

  @Select(WarehouseState.selectedWarehouse)
  warehouse$: Observable<WarehouseViewModel>;

  @Select(ProductState.selectedProduct)
  product$: Observable<ProductViewModel>;

  @Select(InventoryState.warehouseInventories)
  warehouseInventories$: Observable<WarehouseStockPairViewModel[]>;

  @Select(InventoryState.warehouseInventory)
  warehouseInventory$: Observable<StockExpiryPairViewModel[]>;
  code:string;
  userId: string;
  title: string;

  scanForm: FormGroup;
  warehouseId: number;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(private fb: FormBuilder, private store: Store,public dialog: MatDialog) {
    this.title = "Scan Transfer";
    this.unsubscribe$ = new ReplaySubject(1);
    const today = new Date();
    const date = today.getDate();
    const month = today.getMonth();
    const year = today.getFullYear();
    this.scanForm = this.fb.group({
      type: ["Outbound", [Validators.required]],
      sku: ["", [Validators.required]],
      startDate: [new Date(year, month, date), [Validators.required]],
      endDate: [new Date(year, month + 1, date), [Validators.required]],
      any: [false]
    });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next(false);
  }

  ngOnInit() {
    this.store.dispatch([ClearInventoryStateData,ClearProductStateData,ClearTransportStateData]);
    this.warehouseInventories$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(pairs => {
        if (pairs.length > 0) {
          this.store.dispatch(new Navigate({
            commands: ['/transfer/outbound-request', { 
              sku: this.scanForm.value.sku,
              any: this.scanForm.value.any,
              start: this.scanForm.value.startDate,
              end: this.scanForm.value.endDate
            }]
          }))
        }
      });
    this.warehouseInventory$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(inventory => {
        if (inventory !==null) {
          this.store.dispatch(new Navigate({
            commands: ['/transfer/outbound', { sku: this.scanForm.value.sku }]
          }))
        }
      });
    this.product$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(product => {
        if (product !==null) {
          this.store.dispatch(new Navigate({
            commands: ['/transfer/inbound', { sku: this.scanForm.value.sku }]
          }))
        }
      })
    
    this.warehouse$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(vals => {
        if (vals !== null) {
          this.warehouseId = vals.id;
        }
      });

    this.userData$?.pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if (user) {
          this.userId = user.sub;
          this.store.dispatch(new FetchWarehouseByUser(user.sub));
        }
      })
  }
  
  onTransferTypeChanged(res: any) {
    this.scanForm.patchValue({
      type: res.value
    })
  }
  
  openDialog():void {
    const dialogRef = this.dialog.open(ScanBarcodeComponent,{
      width: '250px',
      data: {code: this.code}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.code = result;
      this.scanForm.patchValue({
        sku:this.code
      });
      this.submit();
    });
  }
  submit() {

    if (!this.scanForm.valid) {
      this.store.dispatch(new Alert("Form belum lengkap"));
      return;
    }
    if (this.scanForm.value.type == 'Request Kirim') {
      this.store.dispatch(new FetchAllStockWarehouseByExpiry({
        skuStock: this.scanForm.value.sku,
        startDate: this.scanForm.value.startDate,
        endDate: this.scanForm.value.endDate,
        any: this.scanForm.value.any,
        inquirer: this.warehouseId
      }));
    }
    else if (this.scanForm.value.type == 'Request Terima') {
      this.store.dispatch(new Navigate({
        commands: ['/transfer/inbound-request',{sku:this.scanForm.value.sku}]
      }))
    }
    else if (this.scanForm.value.type == 'Outbound') {
      this.store.dispatch(new FetchAllStockWarehouse({
        skuStock: this.scanForm.value.sku,
        warehouseId: this.warehouseId
      }));
    }
    else if (this.scanForm.value.type == 'Inbound') {
      this.store.dispatch(new FetchProductBySKU(this.scanForm.value.sku));
    }
  }
}