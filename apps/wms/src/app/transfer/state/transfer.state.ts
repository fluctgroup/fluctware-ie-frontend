import { State, Selector, StateContext, Action, Select } from '@ngxs/store';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CoreState, IConfiguration, RegisterLoadingOverlay, Alert, ResolveLoadingOverlay, Navigate, SessionStorageService, Guid } from '@fluctware/core';
import { Observable, ReplaySubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { take, tap, mergeMap, takeUntil } from 'rxjs/operators';
import { Configuration, TransportService, TransportViewModel } from '../../services/warehouse-api';
import { ApproveRequestInboundStock, ApproveRequestOutboundStock, CancelRequestTransportStock, ClearTransportStateData, FetchHistoryTransportByWarehouseId, FetchOngoingTransportByWarehouseId, FetchTransportDetailById, InboundRequestedStock, InboundStock, OutboundRequestedStock, OutboundStock, RejectRequestInboundStock, RejectRequestOutboundStock, RequestInboundStock, RequestOutboundStock, ReturnRequest } from './transfer.actions';

export interface TransferStateModel {
  ongoingTransfer: TransportViewModel[];
  historyTransfer: TransportViewModel[];
  selectedTransfer: TransportViewModel;
}
const defaults: TransferStateModel = {
  ongoingTransfer: [],
  historyTransfer: [],
  selectedTransfer: null
};
@State<TransferStateModel>({
  name: 'Transfer',
  defaults: defaults
})
@Injectable()
export class TransferState {

  @Select(CoreState.configuration)
  configurations$: Observable<IConfiguration>;

  configuration: Configuration;

  unsubscribe$: ReplaySubject<boolean>;
  constructor(
    private httpClient: HttpClient,
    private transferService: TransportService,
    private storageService: SessionStorageService
  ) {
    this.unsubscribe$ = new ReplaySubject(1);

    this.configurations$.pipe(
      takeUntil(this.unsubscribe$),
      tap(x => {
        console.log(x);
        if (x !== undefined && x !== null) {
          this.configuration = new Configuration({
            accessToken: this.storageService.retrieve(
              'authorizationData_wmsspa'
            ),
            basePath: x.warehouseUrl
          });

          this.transferService = new TransportService(
            this.httpClient,
            x.warehouseUrl,
            this.configuration
          );
        }
      })
    ).subscribe();
  }

  @Selector()
  static historyTransfer(state: TransferStateModel) {
    return state.historyTransfer;
  }
  @Selector()
  static ongoingTransfer(state: TransferStateModel) {
    return state.ongoingTransfer;
  }
  @Selector()
  static selectedTransfer(state: TransferStateModel) {
    return state.selectedTransfer;
  }

  @Action(FetchOngoingTransportByWarehouseId)
  fetchOngoingTransportByWarehouseId({ dispatch, patchState }: StateContext<TransferStateModel>,
    { payload }: FetchOngoingTransportByWarehouseId) {

    dispatch(RegisterLoadingOverlay);

    return this.transferService.apiTransportOngoingIdGet(payload).pipe(
      tap(
        result => patchState({
          ongoingTransfer: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }
  @Action(FetchHistoryTransportByWarehouseId)
  fetchHistoryTransportByWarehouseId({ dispatch, patchState }: StateContext<TransferStateModel>,
    { payload }: FetchHistoryTransportByWarehouseId) {

    dispatch(RegisterLoadingOverlay);

    return this.transferService.apiTransportHistoryIdGet(payload).pipe(
      tap(
        result => patchState({
          historyTransfer: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }
  @Action(FetchTransportDetailById)
  fetchTransportDetailById({ dispatch, patchState }: StateContext<TransferStateModel>,
    { payload }: FetchTransportDetailById) {

    dispatch(RegisterLoadingOverlay);

    return this.transferService.apiTransportDetailIdGet(payload).pipe(
      tap(
        result => patchState({
          selectedTransfer: result
        }),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay]);
        }
      )
    );
  }
  @Action(RequestInboundStock)
  requestInboundStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: RequestInboundStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqInboundPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("RequestInboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer']
          })]);
        }
      )
    );
  }
  @Action(ReturnRequest)
  returnRequest({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: ReturnRequest) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqReturnPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("Return Request"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer']
          })]);
        }
      )
    );
  }
  @Action(RequestOutboundStock)
  requestOutboundStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: RequestOutboundStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqOutboundPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("RequestOutboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer']
          })]);
        }
      )
    );
  }
  @Action(ApproveRequestInboundStock)
  approveRequestInboundStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: ApproveRequestInboundStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqInboundApprovePost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("ApproveRequestInboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer']
          })]);
        }
      )
    );
  }
  @Action(ApproveRequestOutboundStock)
  approveRequestOutboundStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: ApproveRequestOutboundStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqOutboundApprovePost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("ApproveRequestOutboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer']
          })]);
        }
      )
    );
  }
  @Action(RejectRequestInboundStock)
  rejectRequestInboundStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: RejectRequestInboundStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqInboundRejectPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("RejectRequestInboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer']
          })]);
        }
      )
    );
  }
  @Action(RejectRequestOutboundStock)
  rejectRequestOutboundStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: RejectRequestOutboundStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqOutboundRejectPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("RejectRequestOutboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer']
          })]);
        }
      )
    );
  }
  @Action(InboundStock)
  inboundStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: InboundStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportInboundPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("InboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer']
          })]);
        }
      )
    );
  }
  @Action(OutboundStock)
  outboundStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: OutboundStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportOutboundPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("OutboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer']
          })]);
        }
      )
    );
  }
  @Action(CancelRequestTransportStock)
  cancelRequestTransportStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: CancelRequestTransportStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqCancelPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("CancelStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer/history']
          })]);
        }
      )
    );
  }
  @Action(OutboundRequestedStock)
  outboundRequestedStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: OutboundRequestedStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqOutboundOutDeliveryPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("OutboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer/history']
          })]);
        }
      )
    );
  }
  @Action(InboundRequestedStock)
  inboundRequestedStock({ dispatch }: StateContext<TransferStateModel>, 
    { payload }: InboundRequestedStock) {
  
    dispatch(RegisterLoadingOverlay);
  
    return this.transferService.apiTransportReqInboundOutDeliveryPost(payload,Guid.newGuid()).pipe(
      tap(
        result => console.log("InboundStock"),
        (err: HttpErrorResponse) =>
          dispatch([
            new Alert(err.error),
            ResolveLoadingOverlay
          ]),
        () => {
          dispatch([ResolveLoadingOverlay, new Navigate({
            commands: ['/transfer/history']
          })]);
        }
      )
    );
  }
  @Action(ClearTransportStateData)
  clearTransportStateData({ patchState }: StateContext<TransferStateModel>) {
  
    patchState({
      ongoingTransfer: [],
      historyTransfer: [],
      selectedTransfer: null
    })
  }
}