import { ApproveRequestInboundStockCommand, ApproveRequestOutboundStockCommand, CancelRequestTransportCommand, InboundRequestedStockCommand, InboundStockCommand, 
  OutboundRequestedStockCommand, 
  OutboundStockCommand, RejectRequestInboundStockCommand, 
  RejectRequestOutboundStockCommand, RequestInboundStockCommand, 
  RequestOutboundStockCommand, 
  ReturnRequestCommand} from "../../services/warehouse-api";

  

export class FetchOngoingTransportByWarehouseId {
  static readonly type = '[WMS] FETCH ONGOING TRANSPORT BY WAREHOUSE ID';
  constructor(public payload: number) { }
}
export class FetchHistoryTransportByWarehouseId {
  static readonly type = '[WMS] FETCH HISTORY TRANSPORT BY WAREHOUSE ID';
  constructor(public payload: number) { }
}
export class FetchTransportDetailById {
  static readonly type = '[WMS] FETCH TRANSPORT DETAIL BY ID';
  constructor(public payload: number) { }
}
export class RequestInboundStock {
  static readonly type = '[WMS] REQUEST INBOUND STOCK';
  constructor(public payload: RequestInboundStockCommand) { }
}
export class InboundRequestedStock {
  static readonly type = '[WMS] INBOUND REQUESTED STOCK';
  constructor(public payload: InboundRequestedStockCommand) { }
}
export class ApproveRequestInboundStock {
  static readonly type = '[WMS] APPROVE REQUEST INBOUND STOCK';
  constructor(public payload: ApproveRequestInboundStockCommand) { }
}
export class RejectRequestInboundStock {
  static readonly type = '[WMS] REJECT REQUEST INBOUND STOCK';
  constructor(public payload: RejectRequestInboundStockCommand) { }
}
export class InboundStock {
  static readonly type = '[WMS] INBOUND STOCK';
  constructor(public payload: InboundStockCommand) { }
}

export class ReturnRequest {
  static readonly type = '[WMS] RETURN REQUEST';
  constructor(public payload: ReturnRequestCommand) { }
}
export class RequestOutboundStock {
  static readonly type = '[WMS] REQUEST OUTBOUND STOCK';
  constructor(public payload: RequestOutboundStockCommand) { }
}
export class OutboundRequestedStock {
  static readonly type = '[WMS] OUTBOUND REQUESTED STOCK';
  constructor(public payload: OutboundRequestedStockCommand) { }
}
export class ApproveRequestOutboundStock {
  static readonly type = '[WMS] APPROVE REQUEST OUTBOUND STOCK';
  constructor(public payload: ApproveRequestOutboundStockCommand) { }
}
export class RejectRequestOutboundStock {
  static readonly type = '[WMS] REJECT REQUEST OUTBOUND STOCK';
  constructor(public payload: RejectRequestOutboundStockCommand) { }
}
export class OutboundStock {
  static readonly type = '[WMS] OUTBOUND STOCK';
  constructor(public payload: OutboundStockCommand) { }
}
export class CancelRequestTransportStock {
  static readonly type = '[WMS] CANCEL REQUEST TRANSPORT STOCK';
  constructor(public payload: CancelRequestTransportCommand) { }
}
export class ClearTransportStateData {
  static readonly type = '[WMS] CLEAR TRANSPORT STATE DATA';
  constructor() { }
}