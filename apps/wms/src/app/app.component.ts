import { Component, Inject, OnDestroy, OnInit, PLATFORM_ID } from "@angular/core";
import { Router } from "@angular/router";
import { CoreFunctionality, CoreState, LoadConfiguration, Navigate, SessionStorageService } from "@fluctware/core"
import { Select, Store } from "@ngxs/store";
import { OidcSecurityService } from "angular-auth-oidc-client";
import { Observable, ReplaySubject } from "rxjs";
import { environment } from "../environments/environment";
import { ConfigurationService } from "./services/configuration/configuration.service";
import { LoadAppSpecificConfiguration } from "./state/app.actions";
@Component({
  selector: "fluctware-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent extends CoreFunctionality implements OnInit, OnDestroy {

  @Select(CoreState.userData)
  userData$: Observable<any>;

  constructor(@Inject(PLATFORM_ID) private platformIds: Object,
  @Inject(ConfigurationService) private configurationServices: ConfigurationService,
  private routers: Router,
  private storageServices: SessionStorageService,
  private stores: Store,
  public oidcSecurityServices: OidcSecurityService) {
  super(platformIds, routers, stores, oidcSecurityServices,
    storageServices);
  this.unsubscribe$ = new ReplaySubject(1);
}

  ngOnInit() {
    this.setHashAuthentication();
    this.init();

    if (!environment.production) {
      console.log("Environment : Development")
      console.log(environment)
      this.stores.dispatch(new LoadConfiguration(environment.configuration));
      this.stores.dispatch(new LoadAppSpecificConfiguration(environment.configuration));
    } else {
      console.log("Environment : Production")
      this.configurationServices.getProcessEnvConfiguration();
    }
  }
  ngOnDestroy(): void {
  }
}
