import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from "@angular/core";
import {AgmCoreModule} from '@agm/core';
import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';

import { AppComponent } from "./app.component";
import { AppRoutingModule } from './app-routing.module';
import { NgxsModule } from "@ngxs/store";
import { AppState } from "./state/app.state";
import { SharedModule } from "@fluctware/shared";
import { CoreModule } from "@fluctware/core";
import { ScanBarcodeComponent } from "./dialog/scan-barcode/scan-barcode.component";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [AppComponent,ScanBarcodeComponent],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB1lXxx_78CQ8Rj1xhnCLDenwv4rNI_THg',
      libraries: ['places']
    }),
    MatGoogleMapsAutocompleteModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    CoreModule,
    SharedModule,
    NgxsModule.forFeature([AppState]),
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
