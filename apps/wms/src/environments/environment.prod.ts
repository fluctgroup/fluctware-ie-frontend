import { IAppSpecificConfiguration } from "../app/models/configuration.model";

export const environment = {
  production: true,
  
  configuration: {
    warehouseUrl:'http://localhost:5101',
    identityUrl: 'http://localhost:5001',
    scope: 'openid profile warehouse',
    clientId: 'wmsspa'
  } as IAppSpecificConfiguration
};
