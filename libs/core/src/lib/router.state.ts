import { State, Action, StateContext } from '@ngxs/store';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { NgZone, Injectable } from '@angular/core';
import { SetPageName } from './core.actions';

export interface RoutingModel {
  commands: Array<any>;
  extras?: NavigationExtras;
  pageName?: string;
}
export interface RouteLinkModel {
  title: string;
  route: string;
  icon: string;
}
const defaults: RoutingModel = {
  commands: [],
  extras: null,
  pageName: ''
}
export class Navigate {
  static readonly type = '[ROUTER] NAVIGATE';
  constructor(public payload: RoutingModel) { }
}

@State<RoutingModel>({
  name: 'router',
  defaults
})
@Injectable()
export class RouterState {
  constructor(private router: Router, public zone: NgZone) { }

  @Action(Navigate)
  changeRoute({ setState,dispatch }: StateContext<string>, action: Navigate) {
    const path = action.payload;
    this.zone.run(() => { this.router.navigate(path.commands, path.extras); });
    setState(path.commands.toString());
    if (action.payload.pageName !== undefined)
      dispatch(new SetPageName(action.payload.pageName));
  }
}
