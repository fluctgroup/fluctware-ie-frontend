import { IConfirmConfig, IAlertConfig } from '@covalent/core/dialogs';
import { MatDialogConfig } from '@angular/material/dialog';

export class Confirm {
  static readonly type = '[APP] CONFIRM';
  constructor(
    public payload: {
      config: IConfirmConfig;
      onAccept: () => void;
      onReject?: () => void;
    }) { }
}
export class Confirmed {
  static readonly type = '[APP] CONFIRMED';
  constructor(public payload: boolean) { }
}

/** Error Occured Event */
export class ErrorOccured {
  static readonly type = '[APP] ERROR OCCURED';
  /**
   *
   * @param payload Error Message
   */
  constructor(public payload: string) { }
}

export class Alert {
  static readonly type = '[APP] ALERT';
  constructor(public payload: string | IAlertConfig) { }
}

export class ClearDialogs {
  static readonly type = '[APP] CLEAR DIALOGS';
  constructor() { }
}

export class OpenCustomDialog {
  static readonly type = '[APP] OPEN CUSTOM DIALOG';
  constructor(public payload: {
    content: any,
    onClose?: () => void,
    config?: MatDialogConfig<any>
  }) { }
}