import { Injectable } from '@angular/core';
import { SessionStorageService } from '../storage/storage.service';
import { Store } from '@ngxs/store';
import { Configured, ResolveLoadingOverlay, Logged, RegisterLoadingOverlay } from '../../core.actions';
import { OidcSecurityService, AuthWellKnownEndpoints, OpenIdConfiguration } from 'angular-auth-oidc-client';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { take, tap } from 'rxjs/operators';
import { Alert } from '../../dialog.actions';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  private storage: SessionStorageService;

  isAuthorized: boolean;
  constructor(
    _storageService: SessionStorageService,
    public oidcSecurityService: OidcSecurityService,
    private store: Store
  ) {
    this.storage = _storageService;
  }

  GetToken(): any {
    return this.storage.retrieve('authorizationData');
  }
  ConfigureAuth(authorityUrl: string, clientId: string, scope: string) {
    const openIDImplicitFlowConfiguration : OpenIdConfiguration= {
      stsServer : authorityUrl,
      redirect_url : location.origin + '/',
      // The Client MUST validate that the aud (audience) Claim contains its client_id value registered at the Issuer identified by the iss (issuer) Claim as an audience.
      // The ID Token MUST be rejected if the ID Token does not list the Client as a valid audience, or if it contains additional audiences not trusted by the Client.
      client_id : clientId,
      response_type : 'id_token token',
      scope : scope,
      start_checksession : false,
      silent_renew : false,
      // silent_renew_url :location.origin + '/silent-renew.html',
      // HTTP 403
      forbidden_route : '/Forbidden',
      // HTTP 401
      unauthorized_route : '/Unauthorized',
      log_console_warning_active : true,
      log_console_debug_active : true,
      // id_token C8: The iat Claim can be used to reject tokens that were issued too far away from the current time,
      // limiting the amount of time that nonces need to be stored to prevent attacks.The acceptable range is Client specific.
      max_id_token_iat_offset_allowed_in_seconds : 60,
      
    }
    const authWellKnownEndpoints : AuthWellKnownEndpoints={
      issuer : 'null',
      jwks_uri :authorityUrl + '/.well-known/openid-configuration/jwks',
      authorization_endpoint :authorityUrl + '/connect/authorize',
      token_endpoint :authorityUrl + '/connect/token',
      userinfo_endpoint :authorityUrl + '/connect/userinfo',
      end_session_endpoint :authorityUrl + '/connect/endsession',
      check_session_iframe :authorityUrl + '/connect/checksession',
      revocation_endpoint :authorityUrl + '/connect/revocation',
      introspection_endpoint :authorityUrl + '/connect/introspect'
    }
    
    this.oidcSecurityService.setupModule(
      openIDImplicitFlowConfiguration,
      authWellKnownEndpoints
    );
    this.store.dispatch(Configured);
    console.log('APP RECONFIGURED');
  }
  Authorize() {
    this.resetAuthorizationData();
    this.oidcSecurityService.authorize();
  }
  SetAccessToken(accessToken:string){
    this.storage.store('authorizationData', accessToken);
    this.store.dispatch(Logged);
  }
  Register(authorityUrl: string, clientId: string, scope: string) {
    this.resetAuthorizationData();
    const controllerUrl = authorityUrl + '/account/register';
    const url = this.GenerateUrl(controllerUrl, clientId, scope);

    window.location.href = url;
  }
  GenerateUrl(controllerUrl: string, clientId: string, scope: string) {
    const redirectUri = location.origin;
    const responseType = 'id_token token';
    const nonce = 'N' + Math.random() + '' + Date.now();
    const state = Date.now() + '' + Math.random();

    this.storage.store('authStateControl', state);
    this.storage.store('authNonce', nonce);

    const encoded = encodeURIComponent('/connect/authorize/callback?' +
      'client_id=' + encodeURIComponent(clientId) + '&' +
      'redirect_uri=' + encodeURIComponent(redirectUri + '/') + '&' +
      'response_type=' + encodeURIComponent(responseType) + '&' +
      'scope=' + encodeURIComponent(scope) + '&' +
      'nonce=' + encodeURIComponent(nonce) + '&' +
      'state=' + encodeURIComponent(state));
    const url =
      controllerUrl + '?returnUrl=' + encoded;

    return url;
  }
  Logoff(authorityUrl: string) {
    const authorizationUrl = authorityUrl + '/connect/endsession';
    const idTokenHint = this.storage.retrieve('authorizationDataIdToken');
    const postLogoutRedirectUri = location.origin + '/';

    const url =
      authorizationUrl +
      '?' +
      'id_token_hint=' +
      encodeURI(idTokenHint) +
      '&' +
      'post_logout_redirect_uri=' +
      encodeURI(postLogoutRedirectUri);

    this.resetAuthorizationData();

    window.location.href = url;
  }

  resetAuthorizationData() {
    this.storage.store('authorizationData', '');
    this.storage.store('authorizationDataIdToken', '');
    this.storage.store('userData', '');
    this.storage.store('session_state', '');
    this.storage.store('authorizationResult', '');
    this.isAuthorized = false;
    this.storage.store('IsAuthorized', false);
  }
}
