/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SuperAdminAuthGuardService } from './super-admin-auth-guard.service';

describe('Service: SuperAdminAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SuperAdminAuthGuardService]
    });
  });

  it('should ...', inject([SuperAdminAuthGuardService], (service: SuperAdminAuthGuardService) => {
    expect(service).toBeTruthy();
  }));
});
