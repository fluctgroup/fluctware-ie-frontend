import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import { NgxsModule, Store } from '@ngxs/store';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthModule } from 'angular-auth-oidc-client';
import { CoreState } from '../../core.state';
import { Navigate } from '../../router.state';
import { Logged } from '../../core.actions';

describe('AuthGuardService', () => {
  let store: Store;
  let service: AuthGuardService;
  let storeSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuardService,
      ],
      imports: [
        NgxsModule.forRoot([CoreState]),
        AuthModule.forRoot(),
        HttpClientModule,
        RouterTestingModule
      ]
    });
    store = TestBed.get(Store);
    service = TestBed.get(AuthGuardService);
  });
  const userDataNoAdminRoles = {
    profile: {
      name: 'firstname',
      last_name: 'lastname',
      email: 'test@enterprise.com',
      roles: ['Test']
    }
  };
  describe('Functionality Test', () => {
    describe('spyCallFake', () => {
      beforeEach(() => {
        storeSpy = spyOn(store, 'dispatch').and.callFake(() => null);
      });
      it('should return false and dispatch navigate if user is not authenticated', () => {
        const result = service.checkAuth();
        expect(result).toBeFalsy();
        expect(store.dispatch).toHaveBeenCalledWith(
          new Navigate({
            commands: ['/not-authorized']
          })
        );
      });
    });
    describe('spyCallThrough', () => {
      beforeEach(() => {
        storeSpy = spyOn(store, 'dispatch').and.callThrough();
      });
      it('should return true when all criteria passed', () => {
        store.dispatch(new Logged(userDataNoAdminRoles));
        const result = service.checkAuth();
        expect(result).toBeTruthy();
      });
    });
  });
});
