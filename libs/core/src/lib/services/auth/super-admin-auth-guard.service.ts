import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { CoreState } from '../../core.state';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Navigate } from '../../router.state';

@Injectable({
  providedIn: 'root'
})
export class SuperAdminAuthGuardService implements CanActivate, CanActivateChild, CanLoad {
  @Select(CoreState.authenticated) isAuthenticated$: Observable<boolean>;

  @Select(CoreState.userData) userData$: Observable<any>;

  constructor(private store: Store) { }

  canActivate(): boolean | Observable<boolean> | Promise<boolean> {
    return this.verifyAuth();
  }
  canActivateChild(): boolean | Observable<boolean> | Promise<boolean> {
    return this.verifyAuth();
  }
  canLoad(): boolean | Observable<boolean> | Promise<boolean> {
    return this.verifyAuth();
  }

  verifyAuth(): boolean {
    let result: boolean;
    this.isAuthenticated$.pipe(take(1)).subscribe(x => (result = x));
    if (!result) {
      this.store.dispatch(
        new Navigate({
          commands: ['/not-authorized']
        })
      );

      return result;
    }
    this.userData$.pipe(take(1)).subscribe(data => {
      if (data.roles === undefined) {
        result = false;
      } else if (!data.roles.includes('superadmin')) {
        result = false;
      } else {
        result = true;
      }
    });
    if (!result) {
      this.store.dispatch(
        new Navigate({
          commands: ['/not-authorized']
        })
      );
    }

    return result;
  }
}
