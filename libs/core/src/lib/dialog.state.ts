import { State, Action, StateContext, Selector } from '@ngxs/store';

import { IConfirmConfig, IAlertConfig, TdDialogService } from '@covalent/core/dialogs';
import {Alert, Confirmed, Confirm, ErrorOccured,ClearDialogs, OpenCustomDialog} from './dialog.actions';
import { take } from 'rxjs/operators';
import { NgZone, Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

export interface DialogStateModel {
  alertMessage: string;
  alertModel: IAlertConfig;
  errorMessage: string;
  confirmModel: IConfirmConfig;
  confirmation: boolean;
}

const defaults: DialogStateModel = {
  alertMessage: '',
  alertModel: null,
  errorMessage: '',
  confirmModel: null,
  confirmation: false
};

@State<DialogStateModel>({
  name: 'dialog',
  defaults: defaults
})
@Injectable()
export class DialogState {
  constructor(private dialogService: TdDialogService,
    private dialog: MatDialog, public zone: NgZone) { }
  //#region Selectors
  @Selector()
  static errorMessage(state: DialogStateModel) {
    return state.errorMessage;
  }
  @Selector()
  static alertMessage(state: DialogStateModel) {
    return state.alertMessage;
  }
  @Selector()
  static alertModel(state: DialogStateModel) {
    return state.alertModel;
  }
  @Selector()
  static confirmModel(state: DialogStateModel) {
    return state.confirmModel;
  }
  @Selector()
  static confirmation(state: DialogStateModel) {
    return state.confirmation;
  }
  //#endregion

  @Action(ErrorOccured)
  errorOccured(
    { patchState, dispatch }: StateContext<DialogStateModel>,
    { payload }: ErrorOccured
  ) {
    if (payload === undefined) {
      payload = 'Backend Error';
    }
    patchState({
      errorMessage: payload
    });

    this.zone.run(x => {
      this.dialogService.openAlert({
        message: payload,
        title: 'Error', //OPTIONAL, hides if not provided
        closeButton: 'OK' //OPTIONAL, defaults to 'CLOSE'
      }).afterClosed()
        .pipe(take(1))
        .subscribe(() =>
          dispatch(ClearDialogs));
    })

  }

  @Action(Confirm)
  confirm(
    { patchState, dispatch }: StateContext<DialogStateModel>,
    { payload }: Confirm
  ) {
    patchState({
      confirmModel: payload.config
    });
    this.zone.run(x => {
      this.dialogService
        .openConfirm(payload.config)
        .afterClosed()
        .pipe(take(1))
        .subscribe((accept: boolean) => {
          if (accept) {
            payload.onAccept();
          } else {
            if (payload.onReject !== null && payload.onReject !== undefined) {
              payload.onReject();
            }
          }
        });
    })

  }
  @Action(Confirmed)
  confirmed(
    { patchState }: StateContext<DialogStateModel>,
    { payload }: Confirmed
  ) {
    patchState({
      confirmation: payload
    });
  }

  @Action(Alert)
  alert(
    { patchState, dispatch }: StateContext<DialogStateModel>,
    { payload }: Alert) {
    this.zone.run(x => {
      if (typeof payload === "string") {
        patchState({
          alertMessage: payload as string
        });
        this.dialogService.openAlert({
          message: payload as string,
          title: 'Alert', //OPTIONAL, hides if not provided
          closeButton: 'OK' //OPTIONAL, defaults to 'CLOSE'
        }).afterClosed()
          .pipe(take(1))
          .subscribe(() =>
            dispatch(ClearDialogs));
      } else {
        patchState({
          alertModel: payload as IAlertConfig
        });
        this.dialogService.openAlert({
          message: payload.message,
          title: payload.title, //OPTIONAL, hides if not provided
          closeButton: payload.closeButton //OPTIONAL, defaults to 'CLOSE'
        }).afterClosed()
          .pipe(take(1))
          .subscribe(() =>
            dispatch(ClearDialogs));
      }
    });

  }
  @Action(ClearDialogs)
  clearDialogs({ patchState }: StateContext<DialogStateModel>) {
    patchState({
      alertMessage: '',
      confirmModel: null,
      errorMessage: ''
    });
  }
  @Action(OpenCustomDialog)
  openCustomDialog({ patchState }: StateContext<DialogStateModel>,
    { payload }: OpenCustomDialog) {
    this.zone.run(x => {
      this.dialog.open(payload.content, payload.config).afterClosed()
        .subscribe(() => {
          if (payload.onClose !== null && payload.onClose !== undefined) {
            payload.onClose();
          }
        });
    });
  }
}
