import { State, Action, StateContext, Selector } from '@ngxs/store';

import {
  Logged,
  Logout,
  Login,
  LoggedOut,
  DispatchAnalyticsEvent,
  Configured,
  Register,
  ConfigureAuth,
  RegisterLoadingOverlay,
  ResolveLoadingOverlay,
  SetPageName,
  LoadConfiguration,
  HandleHTTPException
} from './core.actions';
import { SecurityService } from './services/security/security.service';
import {IConfiguration} from './models/configuration.model';

import {TdLoadingService} from '@covalent/core/loading';
import { Injectable } from '@angular/core';
import { Alert } from '..';
import { SessionStorageService } from './services/storage/storage.service';
import { tap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

export interface CoreStateModel {
  configuration: IConfiguration;
  authenticated: boolean;
  userData: any;
  progressLoading: number;
  isLoading: boolean;
  isReady: boolean;
  pageName: string;
}

const defaults: CoreStateModel = {
  configuration: null,
  authenticated: false,
  userData: null,
  progressLoading: 0,
  isLoading: false,
  isReady: false,
  pageName: ''
};

@State<CoreStateModel>({
  name: 'core',
  defaults: defaults
})
@Injectable()
export class CoreState {

  constructor(
    private http:HttpClient,
    private loadingService: TdLoadingService,
    private securityService: SecurityService,
    private sessionStorageService:SessionStorageService
  ) {
  }
  //#region Selectors
  @Selector()
  static pageName(state: CoreStateModel) {
    return state.pageName;
  }

  @Selector()
  static isLoading(state: CoreStateModel) {
    return state.isLoading;
  }
  @Selector()
  static progressLoading(state: CoreStateModel) {
    return state.progressLoading;
  }
  @Selector()
  static configuration(state: CoreStateModel) {
    return state.configuration;
  }
  @Selector()
  static authenticated(state: CoreStateModel) {
    return state.authenticated;
  }
  @Selector()
  static userData(state: CoreStateModel) {
    return state.userData;
  }
  @Selector()
  static isReady(state: CoreStateModel) {
    return state.isReady;
  }
  //#endregion

  //#region Authentication
  
  @Action(RegisterLoadingOverlay)
  registerLoadingOverlay({ patchState }: StateContext<CoreStateModel>) {
    patchState({ isLoading: true });
    this.loadingService.register();
  }

  @Action(ResolveLoadingOverlay)
  resolveLoadingOverlay({ patchState }: StateContext<CoreStateModel>) {
    patchState({ isLoading: false });
    this.loadingService.resolve();
  }

  /** Login Command */
  @Action(Login)
  login() {
    this.securityService.Authorize();
  }

  /** Register Command */
  @Action(Register)
  register({ getState }: StateContext<CoreStateModel>) {
    const state = getState();
    this.securityService.Register(state.configuration.identityUrl,
      state.configuration.clientId, state.configuration.scope);
  }

  /** Logout Command */
  @Action(Logout)
  logout({ getState }: StateContext<CoreStateModel>) {
    const state = getState();
    this.securityService.Logoff(state.configuration.identityUrl);
  }

  /** Logged Event */
  @Action(Logged)
  logged({ patchState }: StateContext<CoreStateModel>,
         { payload }: Logged) {
    patchState({
      authenticated: true,
      userData: payload
    });
    sessionStorage.removeItem('hash');
  }

  /** Logged Out Event */
  @Action(LoggedOut)
  loggedOut({ patchState }: StateContext<CoreStateModel>) {
    patchState({
      authenticated: false,
      userData: null
    });
  }
  /** SetPageName Event */
  @Action(SetPageName)
  setPageName({ patchState }: StateContext<CoreStateModel>,
    { payload }: SetPageName) {
    patchState({
      pageName: payload
    });
  }
  /** ConfigureAuth Event */
  @Action(ConfigureAuth)
  configureAuth({ getState }: StateContext<CoreStateModel>) {
    const state = getState();

    this.securityService.ConfigureAuth(state.configuration.identityUrl,
      state.configuration.clientId, state.configuration.scope);
  }
  //#endregion

  /** dispatchAnalyticsEvent Command */
  @Action(DispatchAnalyticsEvent)
  dispatchAnalyticsEvent({ }: StateContext<CoreStateModel>, { payload }: DispatchAnalyticsEvent) {
    (window as any).ga('send', 'event', {
      eventCategory: payload.eventCategory,
      eventLabel: payload.eventLabel,
      eventAction: payload.eventAction,
      eventValue: payload.eventValue
    });
  }

  /** configured Command */
  @Action(Configured)
  configured({ patchState }: StateContext<CoreStateModel>) {
    patchState({
      isReady: true
    })
  }

  /** Load Configuration Command */
  @Action(LoadConfiguration)
  loadConfiguration(
      { patchState }: StateContext<CoreStateModel>,
      { payload }: LoadConfiguration
  ) {
      patchState({
          configuration: payload
      });
  }
  
  /** Handle HTTP Exception Command */
  @Action(HandleHTTPException)
  handleHTTPException(
      { dispatch, patchState }: StateContext<CoreStateModel>,
      { payload }: HandleHTTPException
  ) {
    if(payload.status===401){
      if(this.securityService.GetToken()!==""){
        this.securityService.resetAuthorizationData();
      }
      dispatch([new Alert("Session Expired."),ResolveLoadingOverlay]);
    }
    else{
      dispatch([new Alert(payload.error),
      ResolveLoadingOverlay])
    }
  }
}
