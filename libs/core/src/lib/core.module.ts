import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { RouterState } from './router.state';
import { CoreState } from './core.state';
import { SecurityService } from './services/security/security.service';
import { SessionStorageService, LocalStorageService } from './services/storage/storage.service';
import { AuthModule, OidcConfigService } from 'angular-auth-oidc-client';
import { RouterModule } from '@angular/router';
import { NotAuthorizedComponent } from './not-authorized/not-authorized.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DialogState } from './dialog.state';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    NgxsModule.forRoot([RouterState, DialogState,CoreState]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    AuthModule.forRoot()
  ],
  declarations: [
    NotAuthorizedComponent,
    NotFoundComponent
  ],
  exports: [
    NotAuthorizedComponent,
    NotFoundComponent
  ],
  entryComponents: [
    NotAuthorizedComponent,
    NotFoundComponent
  ],
  providers: [
    SecurityService, 
    SessionStorageService,
    LocalStorageService, 
    OidcConfigService]
})
export class CoreModule { }
