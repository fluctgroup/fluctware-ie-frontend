import { AnalyticsEvent } from './models/analytics-event.model';
import { IConfiguration } from './models/configuration.model';
import { HttpErrorResponse } from '@angular/common/http';

/** Register Loading Overlay Command */
export class RegisterLoadingOverlay {
  static readonly type = '[APP] REGISTER LOADING OVERLAY';
  constructor() { }
}

/** Resolve Loading Overlay Command */
export class ResolveLoadingOverlay {
  static readonly type = '[APP] RESOLVE LOADING OVERLAY';
  constructor() { }
}

/** Login command */
export class Login {
  static readonly type = '[APP] LOGIN';
  constructor() { }
}
/** Logged Event */
export class Logged {
  static readonly type = '[APP] LOGGED';
  /** user data */
  constructor(public payload: any) { }
}
/** ConfigureAuth Event */
export class ConfigureAuth {
  static readonly type = '[APP] CONFIGURE AUTH';
  constructor() { }
}

/** Login command */
export class Register {
  static readonly type = '[APP] REGISTER';
  constructor() { }
}

/** Logged Out Event */
export class LoggedOut {
  static readonly type = '[APP] LOGGED OUT';
  constructor() { }
}
/** Logout Command */
export class Logout {
  static readonly type = '[APP] LOGOUT';
  constructor() { }
}

/** DispatchAnalyticsEvent command */
export class DispatchAnalyticsEvent {
  static readonly type = '[APP] DISPATCH ANALYTICS EVENT';
  constructor(public payload: AnalyticsEvent) { }
}

export class Configured {
  static readonly type = '[APP] CONFIGURED';
  constructor() { }
}

export class SetPageName {
  static readonly type = '[APP] SET PAGE NAME';
  constructor(public payload: string) { }
}

/** Load Configuration Command */
export class LoadConfiguration {
  static readonly type = '[APP] LOAD CONFIGURATION';
  constructor(public payload: IConfiguration) { }
}
export class HandleHTTPException {
  static readonly type = '[APP] HANDLE HTTP EXCEPTION';
  constructor(public payload: HttpErrorResponse) { }
}