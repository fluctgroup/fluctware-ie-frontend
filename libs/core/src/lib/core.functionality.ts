import { isPlatformBrowser } from '@angular/common';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { Store, Select } from '@ngxs/store';
import { take } from 'rxjs/operators';
import { Observable, ReplaySubject } from 'rxjs';
import { SessionStorageService } from './services/storage/storage.service';
import {Logged,LoggedOut,ConfigureAuth,Login,Register,Logout, LoadConfiguration} from './core.actions';
import { CoreState } from './core.state';
import { Navigate } from './router.state';
import { IConfiguration } from './models/configuration.model';
import { isDevMode } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

export class CoreFunctionality {
  //#region select states
  @Select(CoreState.configuration) configuration$: Observable<IConfiguration>;
  @Select(CoreState.authenticated) isAuthenticated$: Observable<boolean>;
  @Select(CoreState.isReady) isReady$: Observable<boolean>;
  //#endregion

  unsubscribe$: ReplaySubject<boolean>;

  isTestMode = false;
  configuration: IConfiguration;
  username: string;
  userEmail: string;
  role:string;

  constructor(
    private platformId: Object,
    private router: Router,
    private store: Store,
    private oidcSecurityService: OidcSecurityService,
    private storageService: SessionStorageService
  ) {
    this.unsubscribe$ = new ReplaySubject(1);
  }
  init() {
    if (isPlatformBrowser(this.platformId)) {
      this.configureSettings();
      
      this.router.events.subscribe(evt => {
        if (!(evt instanceof NavigationEnd)) {
          return;
        }

        //#region Google Analytics
        (window as any).ga('set', 'page', evt.urlAfterRedirects);
        (window as any).ga('send', 'pageview');
        //#endregion
      });
      
      this.oidcSecurityService.getIsAuthorized().subscribe(auth => {
        console.log(auth);
      });

      this.oidcSecurityService.getUserData().subscribe(x => {
        if (x) {
          this.store.dispatch([new Logged(x)]);
          this.username = x.first_name + ' ' + x.last_name;
          this.userEmail = x.email;
          this.role=x.roles;
        } else {
          this.store.dispatch(LoggedOut);
          this.username = 'anonymous';
          this.userEmail = 'anonymous@email.com';
          this.role=x.roles;
        }
      });

    }
  }
  doCallbackLogicIfRequired() {
    const hash = this.storageService.retrieve('hash');
    if (hash !== null && hash !== undefined) {
      this.store.dispatch(ConfigureAuth);
      this.oidcSecurityService.authorizedImplicitFlowCallback(hash);
    }
  }
  setHashAuthentication() {
    if (isPlatformBrowser(this.platformId)) {
      if (window.location.hash) {
        this.storageService.store('hash', window.location.hash.substr(1));
      }
    }
  }
  /** configure settings application */
  configureSettings() {
    this.configuration$.subscribe(x => {
      if (x !== null && x !== undefined) {
        this.configuration = x;
        this.store.dispatch(ConfigureAuth);
      }
    });

    if (this.oidcSecurityService.moduleSetup) {
      this.doCallbackLogicIfRequired();
    } else {
      this.oidcSecurityService.onModuleSetup.pipe(take(1)).subscribe(() => {
        this.doCallbackLogicIfRequired();
      });
    }
  }

  onNavigate(url: string) {
    this.store.dispatch(
      new Navigate({
        commands: [url]
      })
    );
  }

  //#region auth
  onLoginBtnClicked() {
    this.store.dispatch(Login);
  }
  onRegisterBtnClicked() {
    this.store.dispatch(Register);
  }
  onLogoutBtnClicked() {
    this.store.dispatch(Logout);
    this.storageService.clear();
  }

  //#endregion

}
