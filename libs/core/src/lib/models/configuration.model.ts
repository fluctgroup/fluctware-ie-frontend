export interface IConfiguration {
  warehouseUrl: string;

  //#region App Urls
  identityUrl: string;

  //#endregion
  scope: string;
  clientId: string;
  materialRecoveryBotApi:string;
}
