export interface AnalyticsEvent {
    eventCategory: string;
    eventLabel: string;
    eventAction: string;
    eventValue?: number;
}
