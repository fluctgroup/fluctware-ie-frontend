import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SidebarMenuModel } from './sidebar-menu.model';
import { SidebarViewModel } from './sidebar.viewmodel';
import { Observable } from 'rxjs';

@Component({
  selector: 'kri-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input()
  sidebarVM: SidebarViewModel;
  @Input()
  isAuthenticated:boolean;

  @Output() navigate: EventEmitter<string>;
  @Output() signIn: EventEmitter<any>;
  @Output() signOut: EventEmitter<any>;

  constructor() {
    this.navigate = new EventEmitter<string>();
    this.signIn = new EventEmitter<any>();
    this.signOut = new EventEmitter<any>();
  }

  ngOnInit() {
  }
  onNavigate(link: string) {
    this.navigate.emit(link);
  }
  onLogoutBtnClicked() {
    this.signOut.emit();
  }
  onLoginBtnClicked() {
    this.signIn.emit();
  }
}
