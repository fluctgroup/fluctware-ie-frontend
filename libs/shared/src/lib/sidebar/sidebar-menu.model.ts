export interface SidebarMenuModel {
  name: string;
  iconUrl: string;
  linkUrl: string;
}
