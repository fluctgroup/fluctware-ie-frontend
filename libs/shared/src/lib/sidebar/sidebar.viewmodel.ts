import { SidebarMenuModel } from './sidebar-menu.model';

export interface SidebarViewModel {
  username:string;
  role:string;
  balance:string;
  menus:SidebarMenuModel[];
  appVersion:string;
}
