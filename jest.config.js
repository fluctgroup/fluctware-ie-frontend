module.exports = {
  projects: [
    "<rootDir>/apps/portal",
    "<rootDir>/apps/wms",
    "<rootDir>/libs/shared",
    "<rootDir>/libs/core",
  ],
};
